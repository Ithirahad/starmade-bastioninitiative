package org.ithirahad.bastioninitiative.util;

import api.common.GameCommon;
import api.utils.draw.ModWorldDrawer;
import api.utils.textures.IconBakeryUtils;
import api.utils.textures.StarLoaderTexture;
import api.utils.textures.TextureSwapper;
import com.bulletphysics.linearmath.Transform;
import org.ithirahad.bastioninitiative.BIElementInfoManager;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Matrix4f;
import org.schema.common.util.StringTools;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.client.view.gui.GUI3DBlockElement;
import org.schema.game.client.view.tools.SingleBlockDrawer;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.schine.graphicsengine.core.*;
import org.schema.schine.graphicsengine.core.settings.EngineSettings;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;

import javax.imageio.ImageIO;
import javax.vecmath.Matrix3f;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import static org.ithirahad.bastioninitiative.BastionInitiative.modInstance;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

/**
 * Credit to TheDerpGamer for this.
 *
 * @author TheDerpGamer
 * @version 1.0 - [03/30/2022]
 */
public class BlockIconImageRenderer extends ModWorldDrawer {

    private final Transform orientation = new Transform();
    private final Transform orientationTmp = new Transform();
    private Matrix3f rot = new Matrix3f();
    private Transform mView = new Transform();
    private FloatBuffer fb = BufferUtils.createFloatBuffer(16);
    private float[] ff = new float[16];

    private boolean initialized = false;

    public BlockIconImageRenderer() {
        orientation.setIdentity();
        orientationTmp.setIdentity();
    }

    @Override
    public void update(Timer timer) {

    }

    @Override
    public void cleanUp() {

    }

    @Override
    public boolean isInvisible() {
        return false;
    }

    @Override
    public void onInit() {
        if(initialized) return;
        initialized = true;
        ArrayList<ElementInformation> blockTypes = new ArrayList<>(elementEntries.values());

		/*
		FrameBufferObjects fbo = new FrameBufferObjects("ModIconBakery", 1024, 1024);
		try {
			fbo.initialize();
		} catch(GLException exception) {
			exception.printStackTrace();
		}
		fbo.enable();
		GL11.glClearColor(0, 0, 0, 0);
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT | GL11.GL_COLOR_BUFFER_BIT);
		GL11.glViewport(0, 0, 1024, 1024);
		GlUtil.glEnable(GL11.GL_LIGHTING);
		GlUtil.glDisable(GL11.GL_DEPTH_TEST);
		GlUtil.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		int x = 0;
		int y = 0;
		 */

        File iconsFolder = new File(getWorldDataPath() + "/block-icons");
        if(!iconsFolder.exists()) iconsFolder.mkdirs();

        for(final ElementInformation blockInfo : blockTypes) {
            if(blockInfo.getName().contains("environmental") || blockInfo.hasLod() || !blockInfo.placable) continue;
            final FrameBufferObjects fbo = new FrameBufferObjects(blockInfo.getName(), 64, 64);
            try {
                fbo.initialize();
            } catch(GLException exception) {
                exception.printStackTrace();
            }
            fbo.enable();
            GL11.glClearColor(0, 0, 0, 0);
            GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT | GL11.GL_COLOR_BUFFER_BIT);
            GL11.glViewport(0, 0, 64, 64);
            GlUtil.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
            GlUtil.glDisable(GL11.GL_DEPTH_TEST);

            GUI3DBlockElement.setMatrix();
            Matrix4f modelviewMatrix = Controller.modelviewMatrix;
            fb.rewind();
            modelviewMatrix.store(fb);
            fb.rewind();
            fb.get(ff);
            mView.setFromOpenGLMatrix(ff);
            mView.origin.set(0, 0, 0);
            GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
            //GUIElement.enableOrthogonal3d(1024, 1024);
            GUIElement.enableOrthogonal3d(64, 64);
            GlUtil.glPushMatrix();
            //GlUtil.translateModelview(32 + (x * 64), 32 + (y * 64), 0f);
            GlUtil.translateModelview(32, 32, 0f);
            GlUtil.scaleModelview(32f, -32f, 32f);
            if(blockInfo.getBlockStyle() == BlockStyle.SPRITE) {
                orientationTmp.basis.set(mView.basis);
                mView.basis.setIdentity();
            } else {
                rot.set(orientation.basis);
                mView.basis.mul(rot);
            }

            GlUtil.glMultMatrix(mView);
            if(blockInfo.getBlockStyle() == BlockStyle.SPRITE) mView.basis.set(orientationTmp.basis);
            SingleBlockDrawer drawer = new SingleBlockDrawer();
            drawer.setLightAll(false);
            GlUtil.glPushMatrix();
            if(blockInfo.getBlockStyle() != BlockStyle.NORMAL) GlUtil.rotateModelview((Float) EngineSettings.ICON_BAKERY_BLOCKSTYLE_ROTATE_DEG.getCurrentState(), 0, 1, 0);
            GlUtil.rotateModelview(45.0f / 2.0f, 1, 0, 0);
            GlUtil.rotateModelview(45, 0, -1, 0);
            if(blockInfo.isController()) GlUtil.rotateModelview(180, 0, 1, 0);
            drawer.drawType(blockInfo.getId());
            GlUtil.glPopMatrix();
            GUIElement.disableOrthogonal();
            AbstractScene.mainLight.draw();
            GlUtil.glDisable(GL11.GL_NORMALIZE);
            GlUtil.glEnable(GL11.GL_DEPTH_TEST);

            try {
                String path = getWorldDataPath() + "/block-icons/" + blockInfo.getName().toLowerCase().replaceAll(" ", "-") + "-icon";
                if(blockInfo.isReactorChamberAny()) {
                    ElementInformation root = ElementKeyMap.getInfo((short) blockInfo.chamberRoot);
                    if(root != null) path = root.getName().toLowerCase().replaceAll(" ", "-") + "-icon";
                }

                final File outputFile = new File(path + ".png");
                if(!outputFile.exists()) outputFile.createNewFile();
                GlUtil.writeScreenToDisk(path, "png", 64, 64, 4, fbo);
                final String finalPath = path;
                StarLoaderTexture.runOnGraphicsThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String textureName = finalPath.substring(finalPath.lastIndexOf("/") + 1);
                            BufferedImage image = ImageIO.read(outputFile);
                            StarLoaderTexture texture = StarLoaderTexture.newIconTexture(image);
                            blockInfo.setBuildIconNum(texture.getTextureId());
                            BufferedImage source = IconBakeryUtils.writeFBOToBufferedImage(64, 64, 4, fbo);
                            StarLoaderTexture target = BIElementInfoManager.getTexture(textureName);
                            TextureSwapper.setIconTexture(source, blockInfo.getBuildIconNum(), target);
                            int sheetNum = blockInfo.buildIconNum % 256;
                            int x = sheetNum % 16;
                            int y = sheetNum / 16;
                            ImageIO.write(source, "png", outputFile);
                            target.res64.getGraphics().drawImage(source, x, y, null);
                            TextureSwapper.swapSpriteTexture(findSprite(blockInfo.getBuildIconNum()), source);
                        } catch(Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                });
            } catch(Exception ex) {
                ex.printStackTrace();
            }

            fbo.disable();
            fbo.cleanUp();
        }
        GL11.glViewport(0, 0, GLFrame.getWidth(), GLFrame.getHeight());

    }

    private Sprite findSprite(int buildIconNum) {
        System.out.println(buildIconNum);
        try {
            int sheetNum = buildIconNum / 256;
            Sprite sheet = TextureSwapper.getSpriteFromName("build-icons-" + StringTools.formatTwoZero(sheetNum) + "-16x16-gui-");
            sheet.setSelectedMultiSprite(buildIconNum % 16);
            File temp = new File(getWorldDataPath() + "/block-icons/build-icons-" + StringTools.formatTwoZero(sheetNum) + "-16x16-gui-.png");
            if(!temp.exists()) temp.createNewFile();
            ImageIO.write(TextureSwapper.getImageFromSprite(sheet), "png", temp);
            return sheet;
        } catch(Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static String getResourcesPath() {
        return modInstance.getSkeleton().getResourcesFolder().getPath().replace('\\', '/');
    }

    public static String getWorldDataPath() {
        String universeName = GameCommon.getUniqueContextId();
        if(! universeName.contains(":")) {
            return getResourcesPath() + "/data/" + universeName;
        } else {
            try {
                System.err.println("[MOD][Bastion Initiative] Attempted to call server function on dedicated client!");
            } catch(Exception ignored) {
            }
            return null;
        }
    }

}

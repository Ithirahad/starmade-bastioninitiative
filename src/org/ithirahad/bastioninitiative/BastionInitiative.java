package org.ithirahad.bastioninitiative;

import api.DebugFile;
import api.common.GameCommon;
import api.config.BlockConfig;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.Event;
import api.listener.events.block.*;
import api.listener.events.controller.ClientInitializeEvent;
import api.listener.events.controller.ServerInitializeEvent;
import api.listener.events.draw.RegisterWorldDrawersEvent;
import api.listener.events.entity.SegmentControllerChangeFactionEvent;
import api.listener.events.entity.SegmentControllerOverheatEvent;
import api.listener.events.entity.ShipJumpEngageEvent;
import api.listener.events.faction.SystemClaimEvent;
import api.listener.events.gui.HudCreateEvent;
import api.listener.events.gui.MainWindowTabAddEvent;
import api.listener.events.network.ClientLoginEvent;
import api.listener.events.register.ManagerContainerRegisterEvent;
import api.listener.events.register.RegisterConfigGroupsEvent;
import api.listener.events.systems.InterdictionCheckEvent;
import api.listener.events.weapon.ExplosionEvent;
import api.listener.events.weapon.MissileHitEvent;
import api.listener.events.world.ServerSendableRemoveEvent;
import api.listener.events.world.WorldSaveEvent;
import api.listener.fastevents.BlockConfigLoadListener;
import api.listener.fastevents.CannonProjectileHitListener;
import api.listener.fastevents.DamageBeamHitListener;
import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.segmentpiece.SegmentPieceDamageListener;
import api.listener.fastevents.segmentpiece.SegmentPieceKilledListener;
import api.listener.fastevents.SectorUpdateListener;
import api.mod.ModSkeleton;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.FileConfiguration;
import api.mod.config.PersistentObjectUtil;
import api.mod.config.SyncedConfigReceiveEvent;
import api.mod.config.SyncedConfigUtil;
import api.network.packets.PacketUtil;
import api.utils.gui.ModGUIHandler;
import api.utils.particle.ModParticleUtil;
import com.bulletphysics.util.ObjectArrayList;
import glossar.GlossarCategory;
import glossar.GlossarEntry;
import glossar.GlossarInit;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import me.iron.WarpSpace.Mod.WarpJumpEvent;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisSubsystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.*;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementCollectionManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementElementManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancerUnit;
import org.ithirahad.bastioninitiative.entitysystems.disruptor.DisruptorElementManager;
import org.ithirahad.bastioninitiative.events.AegisSubsystemRegisterEvent;
import org.ithirahad.bastioninitiative.gui.corepanel.AegisSystemManagementControlManager;
import org.ithirahad.bastioninitiative.gui.corepanel.AegisSystemManagementPanel;
import org.ithirahad.bastioninitiative.gui.corepanel.RecalibrationTimeInput;
import org.ithirahad.bastioninitiative.gui.flighthud.DisruptionProgressHUDBar;
import org.ithirahad.bastioninitiative.gui.map.NexusMapDrawer;
import org.ithirahad.bastioninitiative.gui.old.AegisSystemSimpleInput;
import org.ithirahad.bastioninitiative.gui.territoryInfo.FactionTerritoryTabListener;
import org.ithirahad.bastioninitiative.gui.territoryInfo.TerritoryListControlManager;
import org.ithirahad.bastioninitiative.gui.territoryInfo.TerritoryListPanel;
import org.ithirahad.bastioninitiative.listeners.BIInWarpListener;
import org.ithirahad.bastioninitiative.listeners.BIJumpInterdictListener;
import org.ithirahad.bastioninitiative.listeners.BIJumpListener;
import org.ithirahad.bastioninitiative.listeners.BIWarpJumpListener;
import org.ithirahad.bastioninitiative.network.*;
import org.ithirahad.bastioninitiative.persistence.BastionPersistenceContainer;
import org.ithirahad.bastioninitiative.persistence.SystemControlInfo;
import org.ithirahad.bastioninitiative.persistence.SystemControlPersistenceContainer;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.ithirahad.bastioninitiative.util.BlockIconImageRenderer;
import org.ithirahad.bastioninitiative.vfx.particle.BastionFieldEffects;
import org.ithirahad.bastioninitiative.vfx.particle.ParticleEffectsManager;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.*;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.projectile.ProjectileController;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerSegmentController;
import org.schema.game.common.controller.damage.projectile.ProjectileHandlerSegmentController.ShotHandler;
import org.schema.game.common.controller.damage.projectile.ProjectileParticleContainer;
import org.schema.game.common.controller.elements.BeamState;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.physics.CubeRayCastResult;
import org.schema.game.common.data.player.PersonalBeamHandler;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.config.FactionPointSpendingConfig;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;
import org.schema.schine.resource.ResourceLoader;

import javax.annotation.Nullable;
import javax.vecmath.Vector3f;
import java.io.File;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.*;

import static api.common.GameClient.getClientState;
import static api.listener.fastevents.FastListenerCommon.*;
import static api.mod.ModStarter.justStartedServer;
import static api.mod.StarLoader.getModFromName;
import static api.mod.StarLoader.registerListener;
import static org.ithirahad.bastioninitiative.BICommandRegistrar.registerCommands;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BIElementInfoManager.aegisSubsystemControllers;
import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.*;
import static org.ithirahad.bastioninitiative.listeners.SegmentPieceListenerProvider.getBlockListener;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_HOURS;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_MINUTES;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.readPrivateField;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

public class BastionInitiative extends StarMod {
    /*
    TODO someday:
        -VFX
            * hard sparks at termination and expanding rings (and some binary code flying out as particles?) when hacking
            * falling rings when disruption successful
            * shockwave and hexagons flying everywhere when vuln window opens
        -Discord bot integration for vulnerability windows (your contests and active defense times)
    */
    public static BastionInitiative modInstance;
    public static BastionPersistenceContainer bsiContainer = null;
    public static SystemControlPersistenceContainer sovContainer = null;

    public static boolean warpSpaceIsPresentAndEnabled = false;
    static final boolean DRAW_ICONS = false;
    public static Object2IntOpenHashMap<String> brokenFactionModuleWatchList = new Object2IntOpenHashMap<>();

    public static void main(String[] args){}


    @Override
    public void onBlockConfigLoad(BlockConfig blockConfig) {

    }

    @Override
    public void onResourceLoad(ResourceLoader resourceLoader) {
        super.onResourceLoad(resourceLoader);
    }

    @Override
    public void onUniversalRegistryLoad() {
        super.onUniversalRegistryLoad();
    }

    @Override
    public void onLoad() {
        modInstance = this;
    }

    //TODO: Mod Config (BIConfiguration)

    @SuppressWarnings({"unchecked"})
    @Override
    public void onEnable() {
        ModSkeleton warpSpace = getModFromName("WarpSpace");
        warpSpaceIsPresentAndEnabled = warpSpace != null && warpSpace.isEnabled();

        gameMapListeners.add(new NexusMapDrawer());
        blockConfigLoadListeners.add(new BlockConfigLoadListener() {
            @Override
            public void preBlockConfigLoad(){}
            @Override
            public void postBlockConfigLoad(){
                //BIElementInfoManager.doPostTasks();
                if(!GameCommon.isClientConnectedToServer())loadPersistenceData();
            }
            @Override
            public void onModLoadBlockConfig_POST(StarMod starMod) {
                if(starMod.getSkeleton().getName().equals("Resources ReSourced")){
                    System.err.println("[MOD][Bastion Initiative] Beginning element initialization.");
                    try {
                        BIElementInfoManager.loadElements(modInstance);
                        BIRecipeManager.addRecipes();
                    } catch (Exception ex) {
                        DebugFile.logError(ex,modInstance);
                        ex.printStackTrace();
                    }
                }
            }
            @Override
            public void onModLoadBlockConfig_PRE(StarMod starMod){}
        });

        if(DRAW_ICONS) {
            registerListener(RegisterWorldDrawersEvent.class, new Listener<RegisterWorldDrawersEvent>() {
                @Override
                public void onEvent(RegisterWorldDrawersEvent e) {
                    e.getModDrawables().add(new BlockIconImageRenderer());
                }
            }, modInstance);
        }
        //TODO: Update whenever Derp fixes this

        StarLoader.registerListener(ClientLoginEvent.class, new Listener<ClientLoginEvent>() {
            final FileConfiguration ruleConfig = getConfig(getRuleConfigName());
            {
                updateRuleConfigValues(ruleConfig,true); //just in case
            }

            @Override
            public void onEvent(ClientLoginEvent event) {
                FactionPointSpendingConfig.FACTION_POINTS_PER_CONTROLLED_SYSTEM = 0;
                if(GameServerState.instance != null) SyncedConfigUtil.sendConfigToClient(event.getServerProcessor(), ruleConfig); //TODO: send FoW profile
            }
        }, modInstance);

        StarLoader.registerListener(SegmentControllerOverheatEvent.class, new Listener<SegmentControllerOverheatEvent>() {
            @Override
            public void onEvent(SegmentControllerOverheatEvent e) {
                //TODO Usually non functional: Segment controller loses faction before broken facmod registration.

                if(justStartedServer && !e.getEntity().railController.isDocked() && !e.getEntity().isDocked()) {
                    int attacker = e.getLastDamager().getFactionId();
                    if(attacker != 0) {
                        SegmentController overheatedShip = e.getEntity();
                        Vector3i system = e.getEntity().getSystem(new Vector3i());
                        int rekt = 0; //scrub.
                        if(overheatedShip.getFactionId() != 0) rekt = overheatedShip.getFactionId();
                        else {
                            SystemControlInfo info = sovContainer.getInfo(system);
                            if (info != null){
                                Integer oldFac = brokenFactionModuleWatchList.get(overheatedShip.getUniqueIdentifier());
                                if(oldFac != null) rekt = oldFac;
                            }
                        }
                        if (rekt != 0 && rekt != attacker) { //It's a different faction
                            SystemControlInfo info = sovContainer.getOrCreateInfo(system);
                            info.handleBlockEntityDestruction(e.getEntity(),attacker,rekt);
                            brokenFactionModuleWatchList.remove(overheatedShip.getUniqueIdentifier());
                        }
                    }
                }
            }
        }, modInstance);

        StarLoader.registerListener(SyncedConfigReceiveEvent.class, new Listener<SyncedConfigReceiveEvent>() {
            @Override
            public void onEvent(SyncedConfigReceiveEvent event) {
                FileConfiguration recievedConfig = event.getConfig();
                ModSkeleton spookyScary = recievedConfig.getMod().getSkeleton(); //a mysterious skeleton we receive in a packet - a most unsettling development!

                if(spookyScary.getName().equals(this.getMod().getSkeleton().getName())) {
                   if(recievedConfig.getName().startsWith(RULE_CONFIG_NAME_STEM)) {
                       FileConfiguration localConfig = getConfig(getRuleConfigName());
                       for (String key : recievedConfig.getKeys()) {
                           String val = recievedConfig.getString(key);
                           localConfig.set(key, val);
                           System.err.println("[MOD][BastionInitiative] Received rule config value for " + key + ": " + val);
                       }
                       updateRuleConfigValues(localConfig, true);

                   } else if(recievedConfig.getName().startsWith(BALANCE_CONFIG_NAME_STEM)){
                       FileConfiguration localConfig = getConfig(getBalanceConfigName());
                       for (String key : recievedConfig.getKeys()) {
                           String val = recievedConfig.getString(key);
                           localConfig.set(key, val);
                           System.err.println("[MOD][BastionInitiative] Received balance config value for " + key + ": " + val);
                       }
                       //updateBalanceConfigValues(config,true);
                   }
                }
            }
        }, modInstance);

        registerListener(BlockPublicPermissionEvent.class, new Listener<BlockPublicPermissionEvent>(EventPriority.MONITOR) {
            @Override
            public void onEvent(BlockPublicPermissionEvent e) {
                if(!(e.getSegmentController() instanceof ManagedUsableSegmentController)) return;
                ManagedUsableSegmentController<?> seg = (ManagedUsableSegmentController<?>) e.getSegmentController().railController.getRoot();
                if(seg.getType() != SimpleTransformableSendableObject.EntityType.SHIP) {
                    AegisCore core = (AegisCore) seg.getManagerContainer().getModMCModule(elementEntries.get("Aegis Core").id);
                    if(core != null && core.getRemoteDisruptWindowStart() + BIConfiguration.DISRUPTION_VULNERABILITY_WINDOW_DURATION_BASE > now()){
                        e.setPermission(false);
                        if(e.isServer()) seg.sendServerMessage(Lng.str("Cannot Modify: Root Structure Is Disrupted!"),MESSAGE_TYPE_WARNING);
                    }
                }
            }
        },modInstance);

        registerListener(SegmentPieceSalvageEvent.class, new Listener<SegmentPieceSalvageEvent>(EventPriority.MONITOR) {
            @Override
            public void onEvent(SegmentPieceSalvageEvent e) {
                if(!(e.getSegmentPiece().getSegmentController() instanceof ManagedUsableSegmentController)) return;
                ManagedUsableSegmentController<?> seg = (ManagedUsableSegmentController<?>) e.getSegmentPiece().getSegmentController().railController.getRoot();
                if(seg.getType() != SimpleTransformableSendableObject.EntityType.SHIP) {
                    AegisCore core = (AegisCore) seg.getManagerContainer().getModMCModule(elementEntries.get("Aegis Core").id);
                    if(core != null && core.getRemoteDisruptWindowStart() + BIConfiguration.DISRUPTION_VULNERABILITY_WINDOW_DURATION_BASE > now()){
                        e.setCanceled(true);
                        if(e.isServer()) seg.sendServerMessage(Lng.str("Cannot Salvage: Root Structure Is Disrupted!"),MESSAGE_TYPE_WARNING);
                    } else {
                        BastionField bf = (BastionField) seg.getManagerContainer().getModMCModule(elementEntries.get("Aegis Invulnerability").id);
                        bf = bf.getReferenceModule();
                        if(bf.getCachedActiveStatus()) {
                            e.setCanceled(true);
                            if(e.isServer()) e.getBeamState().getHandler().getBeamShooter().sendServerMessage(Lng.astr("Cannot Salvage: Bastion Field is active!"),MESSAGE_TYPE_WARNING);
                        }
                        else if(seg.getFactionId() != 0 && seg.getFactionId() != e.getBeamState().getHandler().getBeamShooter().getFactionId()){
                            e.setCanceled(true);
                            if(e.isServer()) e.getBeamState().getHandler().getBeamShooter().sendServerMessage(Lng.astr("Cannot Salvage: This Does Not Belong To You!"),MESSAGE_TYPE_WARNING);
                        }
                    }
                }
            }
        },modInstance);

        registerListener(SegmentPieceActivateByPlayer.class, new Listener<SegmentPieceActivateByPlayer>() {
            @Override
            public void onEvent(SegmentPieceActivateByPlayer e) {
            if (!e.isServer()) {
                short type = e.getSegmentPiece().getType();
                long absPosIndex = e.getSegmentPiece().getAbsoluteIndex(); //TODO: with type 4?
                if (type == elementEntries.get("Aegis Core").id) {
                    PacketUtil.sendPacketToServer(new AegisPromptRequest(e.getSegmentPiece().getSegmentController().getUniqueIdentifier(), absPosIndex));
                    e.getSegmentPiece().setActive(!e.getSegmentPiece().isActive()); //cancel activation
                }
                else if(aegisSubsystemControllers.contains(type)){
                    ManagedUsableSegmentController<?> msc = (ManagedUsableSegmentController<?>) e.getSegmentPiece().getSegmentController();
                    AegisSubsystem sys = (AegisSubsystem) msc.getManagerContainer().getModMCModule(type);
                    Long2ObjectMap<AegisEnhancementCollectionManager> cmm = sys.getEnhancerModuleCollection().getCollectionManagersMap();
                    if(cmm != null && cmm.containsKey(absPosIndex)){
                    e.getPlayer().sendClientMessage(Lng.str(
                            "(- " + ElementKeyMap.getInfo(type).name + " -)" +
                            "\r\nEnhancers: " + cmm.get(absPosIndex).getEnhancement()) +
                            "\r\nStatus: " + (sys.isSuppliedAegisCharge() ? "Online" : "Offline"),
                            //TODO: more info?
                        MESSAGE_TYPE_INFO);
                    }
                }
            }
            }
        }, modInstance);

        registerListener(RegisterConfigGroupsEvent.class, new Listener<RegisterConfigGroupsEvent>() {
            @Override
            public void onEvent(RegisterConfigGroupsEvent event) {
                BIStatusEffectManager.createEffects(event); //seems to want to fire twice in singleplayer
            }
        }, modInstance);

        registerListener(ManagerContainerRegisterEvent.class, new Listener<ManagerContainerRegisterEvent>() {
            @Override
            public void onEvent(ManagerContainerRegisterEvent e) {
                if(e.getContainer() == null) return;
                e.getContainer().getModules().add(
                        new ManagerModuleCollection<>(
                                new DisruptorElementManager(e.getSegmentController()),
                                elementEntries.get("Hacking Computer").id,
                                elementEntries.get("Hacking Module").id
                        )
                );
                SegmentController seg = e.getSegmentController();
                if(seg instanceof SpaceStation || seg instanceof Planet || seg instanceof FloatingRockManaged || seg instanceof PlanetIco) {
                    System.out.println("[MOD][BastionInitiative] Adding mod MMCs to stationary entity (" + e.getSegmentController().getType().getName()  + ") " + e.getSegmentController().getRealName());
                    //e.addModuleCollection(new ManagerModuleCollection(new AegisSubsystemCollectionManager(e.getSegmentController()), elementEntries.get("Aegis Invulnerability"), elementEntries.get("Aegis Enhancer")));
                    AegisCore coreMMC = new AegisCore(seg, e.getContainer());
                    e.addModMCModule(coreMMC);
                    e.addModMCModule(new AegisTransmitter(seg, e.getContainer()));
                    e.addModMCModule(new BastionField(seg, e.getContainer()));
                    e.addModMCModule(new StealthInterruptor(seg, e.getContainer()));
                    e.addModMCModule(new StellarNexus(seg, e.getContainer()));
                    e.addModMCModule(new FTLTrap(seg,e.getContainer()));
                    e.addModMCModule(new FTLWall(seg,e.getContainer()));

                    AegisSubsystemRegisterEvent asi = new AegisSubsystemRegisterEvent(seg,e.getContainer());
                    StarLoader.fireEvent(asi,e.isServer());
                    for (Iterator<AegisSubsystem> it = asi.getAddedSubsystems(); it.hasNext();) {
                        AegisSubsystem customSubsys = it.next();
                        e.addModMCModule(customSubsys);
                        aegisSubsystemControllers.add(customSubsys.getBlockId());
                        //redundant after the first time, but I don't think calling contains() is faster and this is a once-per-entity-load thing anyway
                    }

                    for(short subsysCoreID : aegisSubsystemControllers){
                        ElementInformation info = ElementKeyMap.getInfo(subsysCoreID);

                        ManagerModuleCollection<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> managerModuleCollection =
                                new ManagerModuleCollection<>(
                                        new AegisEnhancementElementManager(e.getSegmentController(), info.getId(), elementEntries.get("Aegis Enhancer").id),
                                        info.getId(),
                                        elementEntries.get("Aegis Enhancer").id
                                );

                        e.getContainer().getModules().add(managerModuleCollection);
                        coreMMC.subsystemEnhancerManagerMap.put(subsysCoreID, managerModuleCollection);
                    }
                }
            }
        }, modInstance);

        registerListener(HudCreateEvent.class, new Listener<HudCreateEvent>() {
            @Override
            public void onEvent(HudCreateEvent ev) { //TODO: move this to the GUI instance container?
                temporaryAegisSystemDialog = new AegisSystemSimpleInput();
                ModGUIHandler.registerNewInputDialog(modInstance.getSkeleton(), temporaryAegisSystemDialog); //TODO remove

                aegisSystemUIControlManager = new AegisSystemManagementControlManager(getClientState());
                aegisSystemPanel = new AegisSystemManagementPanel(getClientState());
                ModGUIHandler.registerNewControlManager(modInstance.getSkeleton(), aegisSystemUIControlManager);

                recalibrationTimeDialog = new RecalibrationTimeInput();
                ModGUIHandler.registerNewInputDialog(modInstance.getSkeleton(), recalibrationTimeDialog);

                DisruptionProgressHUDBar currentEntityReactorBar = new DisruptionProgressHUDBar(ev.getInputState());
                disruptionDatalinkBarHUD = currentEntityReactorBar;
                ev.addElement(currentEntityReactorBar);

                territoryInfoControlManager = new TerritoryListControlManager(getClientState());
                territoryInfoPanel = new TerritoryListPanel(ev.getInputState());
                ModGUIHandler.registerNewControlManager(modInstance.getSkeleton(), territoryInfoControlManager);
            }
        }, modInstance);

        registerListener(MainWindowTabAddEvent.class, new FactionTerritoryTabListener(),this);



        registerListener(SystemClaimEvent.class, new Listener<SystemClaimEvent>() {
            @Override
            public void onEvent(SystemClaimEvent e) {
                if(e.getFaction() != null && e.getFaction().isPlayerFaction() && justStartedServer) {
                    int initiatorID = e.getFaction().getIdFaction();
                    Vector3i pos = e.getSystem().getPos();
                    if(!sovContainer.hasFactionAt(pos, initiatorID) || sovContainer.getOrCreateInfo(pos).getControllingFactionID() != initiatorID) {
                        Faction initiator = GameServerState.instance.getFactionManager().getFaction(e.getOwnershipChange().factionId);
                        int ownerID = sovContainer.getSystemOwner(pos);
                        String owner;
                        if(ownerID == 0) owner = "the ragtag bands of pirates, outlaws, and independent traders that currently call the area \"home\".";
                        else owner = GameServerState.instance.getFactionManager().getFactionName(ownerID);

                        String message = "You have no rights to claim the " + BIUtils.getSystemName(e.getSystem()) + " system!\r\n" +
                                "It currently belongs to " + owner + ".\r\n" +
                                "Deploy more infrastructure and active starship fleets throughout the system, eliminate enemies,\r\n" +
                                "and earn more glory and influence within the system for your faction, and it will belong to you automatically.";
                        for(PlayerState player : initiator.getOnlinePlayers()) if(player.getCurrentSector().equals(e.getSector().pos)){
                            player.sendServerMessage(Lng.astr(message),MESSAGE_TYPE_WARNING);
                        }
                        e.setCanceled(true);
                    }
                }
            }
        }, modInstance);

        sectorUpdateListeners.add(new SectorUpdateListener() {
            @Override
            public void local_preUpdate(Sector sector, Timer timer) {

            }

            @Override
            public void local_activeUpdate(Sector sector, Timer timer) {

            }

            @Override
            public void local_postUpdate(Sector sector, Timer timer) {
                if (justStartedServer) {
                    for (SimpleTransformableSendableObject<?> obj : sector.getEntities()) {
                        if (obj instanceof SpaceStation || obj instanceof Planet) {
                            Vector3i system = sector._getSystem().getPos();
                            SystemControlInfo current;
                            if (obj.getFactionId() == 0) current = sovContainer.getInfo(system);
                            else current = sovContainer.getOrCreateInfo(system);
                            if (current != null) {
                                current.addOrUpdateTrackedStructure((SegmentController) obj);
                            }
                        }
                    }
                }
            }

            @Override
            public void remote_preUpdate(RemoteSector remoteSector, Timer timer) {

            }

            @Override
            public void remote_postUpdate(RemoteSector remoteSector, Timer timer) {

            }
        });

        registerListener(ServerSendableRemoveEvent.class, new Listener<ServerSendableRemoveEvent>() {
            @Override
            public void onEvent(ServerSendableRemoveEvent e) {
                if(e.getSendable() instanceof ManagedUsableSegmentController<?>){
                    ManagedUsableSegmentController<?> entity = (ManagedUsableSegmentController<?>) e.getSendable();
                    if(entity.getType() != SimpleTransformableSendableObject.EntityType.SHIP){
                        AegisCore a = (AegisCore) entity.getManagerContainer().getModMCModule(elementEntries.get("Aegis Core").id);
                        if(a != null)
                            a.handleRemove(a.getBlock());
                    }
                    SystemControlInfo info = sovContainer.getInfo(((ManagedUsableSegmentController<?>) e.getSendable()).getSystem(new Vector3i()));
                    info.handleBlockEntityDestruction(entity,0,((ManagedUsableSegmentController<?>) e.getSendable()).getFactionId());
                    brokenFactionModuleWatchList.remove(entity.getUniqueIdentifier());
                }
            }
        }, modInstance);

        segmentPieceDamageListeners.add(new SegmentPieceDamageListener() {
            @Override
            public int onBlockDamage(SegmentController segmentController, long l, short i, int damage, DamageDealerType damageDealerType, Damager damager, boolean b) {
                if(BastionField.isSegmentControllerInvulnerable(segmentController))
                    return 0;
                else return damage;
            }
        });

        damageBeamHitListeners.add(new DamageBeamHitListener() {
              @Override
              public void handle(BeamState beamState, int hits, BeamHandlerContainer<?> beamHandlerContainer, SegmentPiece segmentPiece, Vector3f vector3f, Vector3f vector3f1, Timer timer, Collection<Segment> collection, DamageBeamHitHandlerSegmentController dbhSeg) {
                  if (segmentPiece.getSegmentController().isOnServer() && segmentPiece.getSegmentController() instanceof ManagedUsableSegmentController<?>) {
                      ManagedUsableSegmentController<?> target = (ManagedUsableSegmentController<?>) segmentPiece.getSegmentController();
                      if (BastionField.isSegmentControllerInvulnerable(target)) {
                          if(beamState.beamType == PersonalBeamHandler.TORCH){
                              beamState.setPower(0.0f);
                              beamState.beamLength = 0.0f;
                              beamState.getHandler().clearStates();
                          }
                          //TODO: config.
                          // Since astronauts are so easy to kill *anyway*, it may be desirable to allow the torch to fire through the Bastion shield.
                          if(target.isOnServer()) BastionFieldEffects.FireEffectServer(target.getSectorId(),target.getSector(new Vector3i()),segmentPiece.getWorldPos(new Vector3f(),target.getSectorId()),beamState.getPower(),false);
                          Object shooterObj = beamHandlerContainer.getHandler().getBeamShooter();
                          if(shooterObj instanceof SegmentController) {
                              SegmentController shooter = (SegmentController) shooterObj;
                              BastionField bastionField = (BastionField) ((ManagedSegmentController<?>)target.railController.getRoot()).getManagerContainer().getModMCModule(elementEntries.get("Aegis Invulnerability").id);
                              bastionField = bastionField.getReferenceModule();
                              for (PlayerState player : ((PlayerControllable) shooter).getAttachedPlayers()) {
                                  bastionField.sendInvulnerabilityMessageIfReady(player);
                              }
                          }
                      }
                  }
              }
        });

        cannonProjectileHitListeners.add(new CannonProjectileHitListener() {
            @Override
            public ProjectileController.ProjectileHandleState handle(Damager damager, ProjectileController c, Vector3f vector3f, Vector3f vector3f1, ProjectileParticleContainer projectileParticleContainer, int i, CubeRayCastResult cubeRayCastResult, ProjectileHandlerSegmentController projectileHandlerSegmentController) {
                return null;
            }

            @Override
            public ProjectileController.ProjectileHandleState handleBefore(Damager damager, ProjectileController projectileController, Vector3f vector3f, Vector3f vector3f1, ProjectileParticleContainer projectileParticleContainer, int i, CubeRayCastResult cubeRayCastResult, ProjectileHandlerSegmentController projectileHandlerSegmentController) {
                return null;
            }

            @Override
            public ProjectileController.ProjectileHandleState handleAfterIfNotStopped(Damager damager, ProjectileController projectileController, Vector3f vector3f, Vector3f vector3f1, ProjectileParticleContainer projectileParticleContainer, int i, CubeRayCastResult cubeRayCastResult, ProjectileHandlerSegmentController projectileHandlerSegmentController) {
                return null;
            }

            @Override
            public void handleAfterAlways(Damager damager, ProjectileController projectileController, Vector3f vector3f, Vector3f vector3f1, ProjectileParticleContainer projectileParticleContainer, int i, CubeRayCastResult cubeRayCastResult, ProjectileHandlerSegmentController projHandlerSegCon) {
                Segment segment = cubeRayCastResult.getSegment();
                if (segment.getSegmentController() instanceof ManagedUsableSegmentController<?>) {
                    ManagedUsableSegmentController<?> target = (ManagedUsableSegmentController<?>) segment.getSegmentController();
                    if (BastionField.isSegmentControllerInvulnerable(target)) {
                        ShotHandler h = (ShotHandler) readPrivateField("shotHandler",projHandlerSegCon);
                        if(target.isOnServer()) BastionFieldEffects.FireEffectServer(target.getSectorId(),target.getSector(new Vector3i()),cubeRayCastResult.hitPointWorld, h.initialDamage,false);
                        if(damager.isSegmentController()) {
                            SegmentController shooter = (SegmentController) damager;
                            BastionField bastionField = (BastionField) ((ManagedSegmentController<?>)target.railController.getRoot()).getManagerContainer().getModMCModule(elementEntries.get("Aegis Invulnerability").id);
                            bastionField = bastionField.getReferenceModule();
                            for (PlayerState player : ((PlayerControllable) shooter).getAttachedPlayers()) {
                                bastionField.sendInvulnerabilityMessageIfReady(player);
                            }
                        }
                    }
                }
            }
        });

        registerListener(MissileHitEvent.class, new Listener<MissileHitEvent>() {
            @Override
            public void onEvent(MissileHitEvent e) {
                Segment segment = e.getRaycast().getSegment();
                if (segment.getSegmentController() instanceof ManagedUsableSegmentController<?>) {
                    ManagedUsableSegmentController<?> target = (ManagedUsableSegmentController<?>) segment.getSegmentController();
                    if (BastionField.isSegmentControllerInvulnerable(target)) {
                        if(target.isOnServer()) BastionFieldEffects.FireEffectServer(target.getSectorId(),target.getSector(new Vector3i()),e.getExplosionData().centerOfExplosion.origin, e.getMissile().getDamage(),false);
                        if(e.getMissile().getShootingEntity().isSegmentController()) {
                            SegmentController shooter = (SegmentController) e.getMissile().getShootingEntity();
                            BastionField bastionField = (BastionField) ((ManagedSegmentController<?>)target.railController.getRoot()).getManagerContainer().getModMCModule(elementEntries.get("Aegis Invulnerability").id);
                            for (PlayerState player : ((PlayerControllable) shooter).getAttachedPlayers()) {
                                bastionField.sendInvulnerabilityMessageIfReady(player);
                            }
                        }
                    }
                }
            }
        }, modInstance);

        registerListener(ExplosionEvent.class, new Listener<ExplosionEvent>() {
            @Override
            public void onEvent(ExplosionEvent e) {
                try {
                    Iterable<Sendable> hitThings = (Iterable<Sendable>) readPrivateField("hitBuffer", e.getExplosionRunnable());
                    for (Iterator<Sendable> iterator = hitThings.iterator(); iterator.hasNext(); ) {
                        Sendable s = iterator.next();
                        if (s instanceof SegmentController) {
                            SegmentController entity = (SegmentController) s;
                            if (BastionField.isSegmentControllerInvulnerable(entity)) iterator.remove();
                        }
                    }
                } catch (Exception ex){
                    modInstance.logException("[ERROR] Failed to cancel explosion damage to protected entities...",ex);
                }
            }
        }, modInstance);

        registerListener(WorldSaveEvent.class, new Listener<WorldSaveEvent>() {
            @Override
            public void onEvent(WorldSaveEvent e) {
                if(e.getCondition() == Event.Condition.POST && !GameCommon.isClientConnectedToServer()) savePersistenceData();
            }
        },this);

        registerListener(SegmentPieceAddEvent.class, (Listener<SegmentPieceAddEvent>) Objects.requireNonNull(getBlockListener(SegmentPieceAddEvent.class)), modInstance);
        registerListener(SegmentPieceAddByMetadataEvent.class, (Listener<SegmentPieceAddByMetadataEvent>) Objects.requireNonNull(getBlockListener(SegmentPieceAddByMetadataEvent.class)), modInstance);

        if(warpSpaceIsPresentAndEnabled) {
            registerListener(WarpJumpEvent.class, new BIWarpJumpListener(), this);
            thrusterElementManagerListeners.add(new BIInWarpListener()); //TODO: migrate to vanilla sector change listener; this is a horrible waste and might not even fire when it's supposed to
        } else {
            registerListener(ShipJumpEngageEvent.class, new BIJumpListener(), this);
            registerListener(InterdictionCheckEvent.class, new BIJumpInterdictListener(), this);
        }

        registerListener(SegmentControllerChangeFactionEvent.class, new Listener<SegmentControllerChangeFactionEvent>() {
            @Override
            public void onEvent(SegmentControllerChangeFactionEvent e) {
                SegmentController entity = (SegmentController) e.getEntity();
                if(e.getOldFaction() != 0 && e.getNewFaction() == 0){
                    if(e.getOldFaction() == -1){
                        SystemControlInfo systemControl = sovContainer.getOrCreateInfo(e.getEntity().getSystem(new Vector3i()));
                        systemControl.addOrUpdateTrackedStructure(entity);
                        //TODO handle pirate station kill
                    }
                    brokenFactionModuleWatchList.add(entity.getUniqueIdentifier(),e.getOldFaction());
                }
                else if(e.getOldFaction() == 0){
                    if(brokenFactionModuleWatchList.containsKey(entity.getUniqueIdentifier())){
                        //handle takeover
                        SystemControlInfo systemControl = sovContainer.getOrCreateInfo(e.getEntity().getSystem(new Vector3i()));
                        systemControl.handleEntityCaptured(entity,e.getNewFaction(),brokenFactionModuleWatchList.get(entity.getUniqueIdentifier()));
                    }
                    brokenFactionModuleWatchList.remove(entity.getUniqueIdentifier());
                }
            }
        },modInstance);

        PacketUtil.registerPacket(AegisUIInputPacket.class);
        PacketUtil.registerPacket(AegisSystemUIPromptPacket.class);
        PacketUtil.registerPacket(AegisPromptRequest.class);
        PacketUtil.registerPacket(DisruptionHUDUpdatePacket.class);
        PacketUtil.registerPacket(DisruptionFXRemoteExecutePacket.class); //TODO: method for particle manager to register its own packets
        PacketUtil.registerPacket(HexBurstFXRemoteExecutePacket.class);
        PacketUtil.registerPacket(NexusMapPacket.class);
        PacketUtil.registerPacket(NexusMapRequest.class);
        PacketUtil.registerPacket(TerritoryInfoPacket.class);
        PacketUtil.registerPacket(TerritoryInfoRequest.class);
        registerCommands();
    }

    private void loadPersistenceData() {
        ArrayList<Object> retrievedObjects = PersistentObjectUtil.getObjects(modInstance.getSkeleton(), BastionPersistenceContainer.class);
        if (retrievedObjects.isEmpty()) {
            if(bsiContainer == null) bsiContainer = new BastionPersistenceContainer();
            if(sovContainer == null) sovContainer = new SystemControlPersistenceContainer();
            PersistentObjectUtil.addObject(modInstance.getSkeleton(), bsiContainer);
            PersistentObjectUtil.addObject(modInstance.getSkeleton(), sovContainer);
        }
        else {
            System.err.println("[MOD][Bastion Initiative] Loading persistent information from previous session...");
            bsiContainer = new BastionPersistenceContainer();
            for(Object obj : retrievedObjects) {
                try {
                    if(obj instanceof BastionPersistenceContainer) {
                        bsiContainer = (BastionPersistenceContainer) obj;
                        bsiContainer.afterDeserialize();
                    }
                    else if(obj instanceof SystemControlPersistenceContainer){
                        sovContainer = (SystemControlPersistenceContainer) obj;
                        sovContainer.afterDeserialize();
                    }
                } catch (Exception fubar) {
                    System.err.println("[MOD][Bastion Initiative][ERROR] Unable to reload persistent data! This may have been caused by a mod update, a corrupted persistence file, or an internal mod error.\n" +
                            "[MOD][Bastion Initiative] All aegis-protected entities may have to be physically reloaded in order to resume normal function, and system ownership data may be lost.");
                    fubar.printStackTrace();
                    System.err.println("[MOD][Bastion Initiative] Exporting error info.");
                    try {
                        File pstLog = new File("moddata\\The Bastion Initiative\\PersistenceError.txt");
                        pstLog.createNewFile();
                        OutputStreamWriter l = new OutputStreamWriter(Files.newOutputStream(pstLog.toPath()));

                        l.append('[' + (new Date().toString()) + "] \n");
                        for (StackTraceElement e : fubar.getStackTrace()) l.append(e.toString() + '\n');

                        l.flush();
                        l.close();
                    } catch (Exception aaa) {
                        System.err.println("[MOD][Bastion Initiative][ERROR] Could not export persistence error information.");
                        aaa.printStackTrace();
                    }
                }
            }
            if(bsiContainer == null){
                bsiContainer = new BastionPersistenceContainer();
                System.err.println("[MOD][Bastion Initiative][WARNING] Created new Aegis System persistence info as contingency. Any previous Aegis System info is lost.");
                PersistentObjectUtil.addObject(modInstance.getSkeleton(), bsiContainer);
            }
            if(sovContainer == null){
                sovContainer = new SystemControlPersistenceContainer();
                System.err.println("[MOD][Bastion Initiative][WARNING] Created new system ownership info as contingency. Any previous system ownership info is lost.");
                PersistentObjectUtil.addObject(modInstance.getSkeleton(), sovContainer);
            }
        }
    }

    @Override
    public void onDisable() {
        if(!GameCommon.isClientConnectedToServer()) savePersistenceData();
    }

    private void savePersistenceData() {
        bsiContainer.beforeSerialize();
        sovContainer.beforeSerialize();
        PersistentObjectUtil.save(modInstance.getSkeleton());
    }

    @Override
    public void onServerCreated(ServerInitializeEvent serverInitializeEvent) {
        super.onServerCreated(serverInitializeEvent);
    }

    @Override
    public void onClientCreated(ClientInitializeEvent e) {
        super.onClientCreated(e);
        addModGlossarEntries();
    }

    @Override
    public void onLoadModParticles(ModParticleUtil.LoadEvent loadEvent) {
        super.onLoadModParticles(loadEvent);
        ParticleEffectsManager.init(loadEvent, modInstance);
        //TODO: disruption datalink effect
        //TODO: stealth breaker wave effect (concentric flat rings + floaty vertical line things clustered around the perimeter of the station's largest dimensions, like STO)
    }

    private void addModGlossarEntries() {
        StringBuilder subsystemListBuilder = new StringBuilder("Aegis Subsystems are blocks with powerful defensive capabilities, that are controlled by the Aegis Core and provided with Aegis Charge via link. Press C on the Aegis Core, then V on the subsystem controller, to attach it.\n" +
                "You can also enhance the functionality of subsystems by linking Aegis Subsystem Enhancers to them, at the cost of greater Aegis Charge consumption rates." +
                "\n\nThe following Aegis Subsystem types are available:");
        String[] subsystemTypes = new String[]{"Bastion Field:\nThe Bastion Field provides invulnerability to a station or planet, at the cost of making the Aegis system susceptible to the Disruption Datalink. The field will also protect docked ships and INACTIVE turrets.\n" +
                "Any turrets with active AI will NOT be protected by the Bastion Field; however, they will still be covered by standard Shields if present." +
                "Bastion Fields require more Aegis Charge while protecting certain blocks, such as Cargo Spaces and weapons. Terrain, decorative blocks, logic, and other non-functional blocks do not increase Charge requirements.",

                "FTL Interceptor:\nThe FTL Interceptor will redirect enemy and neutral ships utilizing " + (warpSpaceIsPresentAndEnabled?"Warp Space":"Jump Drive") + " travel within its radius.\n" +
                "Affected ships will be pulled out of FTL and deposited in the sector of the FTL Interceptor system. The FTL Interceptor does not affect Warp Gates.",

                "FTL Repeller:\nThe FTL Repeller will redirect enemy and neutral ships utilizing " + (warpSpaceIsPresentAndEnabled?"Warp Space":"Jump Drive") + " travel into or within its radius.\n" +
                "Affected ships will be pulled out of FTL and redirected to the edge of the Repeller's area of effect. Ships attempting to enter via FTL from outside the radius will be stopped where their path crosses the edge of the Repeller field, while ships trying to jump between sectors inside the radius will be ejected or \"bounced\" to the edge of the field nearest to their location.\n" +
                "Like the Interceptor, the Repeller does not affect Warp Gates.",

                "Stealth Interruptor:\nThe Stealth Interruptor, as its name suggests, sends out pulses of energy that disable neutral or enemy stealth systems within range.\n" +
                "This includes standard Stealth, Radar Jamming, and Cloaking, and ignores Stealth Levels. Linking Aegis Subsystem Enhancers increases the range of the subsystem."
        };
        for (String str : subsystemTypes) {
            subsystemListBuilder.append("\n\n").append(str);
        }
        System.out.println(subsystemListBuilder);


        GlossarInit.initGlossar(this);
        GlossarCategory cat = new GlossarCategory("The Bastion Initiative");
        cat.addEntry(new GlossarEntry("Introduction","The Bastion Initiative provides your stations, planets, and asteroid bases with new defensive options." +
                "\n\n" +
                "Building upon the resource mechanics introduced by Resources ReSourced, The Bastion Initiative allows you to utilize your stockpiles of renewable resources to fortify your assets and infrastructure with potent defensive Aegis Systems.\n" +
                "So long as you can provide the necessary fuel and firepower to ward off would-be attackers, your once-endless woes of wrecked warpgates, ransacked refineries, hollowed-out harvesting facilities, cratered cities, and trashed trading posts may be a thing of the past.\n\n" +
                "NOTE: If you are not familiar with the Resources ReSourced mod, consult that section of your Glossar first. Resources ReSourced constitutes a massive overhaul of gathering resources and crafting structures and ships, and understanding its systems is vital in order to get started in this universe, and to make use of The Bastion Initiative's additions.")); //TODO: Update this with faction stuff when that update comes

        cat.addEntry(new GlossarEntry("Structure Defense Chambers",
        "In addition to the unique Aegis System that forms the core functionality of The Bastion Initiative, there are also some powerful new chambers available to add to your base.\n" +
                "These chambers are not available on ships, and can only be activated on Space Stations, Planets, and Asteroids.\n\n" +
                "Fortress Shield Amplification:\n Immensely enhances your structure's Shield Capacity, with no penalty to shield upkeep requirements.\n\n" +
                "Fortress Weapons Amplification: Increases the range of weapons and weapon turrets mounted on your structure."));

        cat.addEntry(new GlossarEntry("Castellium & Aegis Cells","Aegis Cells are vital to your imperial endeavours. Therefore, any aspiring Maker among the Stars must know how to obtain them.\n\n" +
                "Castellium:\nCastellium is a new resource introduced by The Bastion Initiative. In the standard Capsule Refinery, raw Castellium can be refined into Castellium Capsules, which are in turn utilized in the manufacture of Aegis Cells.\n\n" +
                "Castellium Centrifuge:\nThe Castellium Centrifuge is a specialized refinery block that produces Castellium. It can convert any fluid resource (Thermyn Amalgam, Parsyne Plasma, or Anbaric Vapor) from either raw or capsule form into units of raw Castellium.\n\n" +
                "Field-Stabilized Canister:\nThe Field-Stabilized Canister is a unique Component used for handling Aegis Charge. It is used in the manufacture of all Aegis Subsystems, as well as to create Aegis Cells and Disruption Datalink computers.\n\n" +
                "Aegis Cell:\nAegis Cells are the \"fuel\" for your Aegis Systems and Disruption Datalinks, and are produced in the Block Assembler. They are made from Field-Stabilized Canisters, standard Energy Cell components from Resources Resourced, and Castellium Capsules."));

        cat.addEntry(new GlossarEntry("The Aegis Core & Aegis Charge", "Aegis Core:\nThe Aegis Core has no defensive abilities of its own, but functions as a control hub for your Aegis Systems and a storage for the Aegis Charge they require in order to function.\n" +
                "The Aegis Core will draw Aegis Cells from any linked Cargo Storage, converting them to Charge.\nNOTE: Placing more than one Aegis Core on your structure may cause bugs and is not recommended.\n\n" +
                "Activating the Aegis Core will bring up an control interface. This menu provides information about the current status of the system and any linked subsystems, and allows you to control the system's functions.\n" +
                "An Aegis System can be in the following states:\n\n"+
                "OFFLINE: \nThe Aegis System is dormant. Subsystems will not function, and no Aegis Charge is consumed. Linking, unlinking, or destroying a Subsystem will revert an Aegis System to the Offline state.\n\n"+
                "AWAITING CHARGE: \nAegis Systems require a large amount of Aegis Charge to initially turn on, depending on the needs of the connected subsystems.\n\n"+
                "ONLINE: \nThe Aegis System is fully provisioned, and functioning normally.\n\n"+
                "OUT OF CHARGE: \nThe Aegis System is out of gas! All subsystems are disabled, but providing enough Aegis Cells via a linked storage will get it up and running again.\n\n"+
                "ONLINE (Disrupted): \nThe Aegis System is functional, but has been affected by a Disruption Datalink, and will go offline during its next recalibration phase.\n\n"+
                "VULNERABLE: \nThe Aegis System is disabled by disruption, and all subsystems are rendered non-functional, leaving the structure vulnerable to attack until the recalibration phase is complete. \nThis is your enemies' chance to strike! (Or your own...)\n\n" +
                "If the system is not disrupted, you may use the Aegis Core UI to change the time of day at which the recalibration phase begins. However, there is a long cooldown on changing the calibration time, so choose wisely."));

        cat.addEntry(new GlossarEntry("Aegis Subsystems",subsystemListBuilder.toString()));

        cat.addEntry(new GlossarEntry("Aegis System Disruption", "As mentioned above, the invulnerability offered by the Bastion Field comes with a drawback: vulnerability to Disruption.\n" +
                "The Disruption Datalink is a beam \"weapon\" which does no damage, but allows an attacker to compromise the Bastion Field's protection along with disabling most other subsystems.\n" +
                "However, this is not immediate. The Disruption process takes a fairly long period, and the structure will subsequently remain invulnerable until its next daily recalibration period, the time of which is chosen by the structure owner.\n\n"+
                "When the beam is fired, the Disruption Datalink Computer will consume Aegis Cells from any linked Cargo Storage in order to operate. A green bar in the centre of your flight HUD will indicate Disruption progress.\n" +
                "If disruption is successful, your faction and the structure owner's faction will be notified of the event via an automatic Faction News post, including the time at which the system will become vulnerable.\n\n"+
                "Like any other type of beam, the Disruption Datalink can be scaled up by adding more Disruption Datalink Modules to a group. Larger disruption beams will be able to Disrupt larger Aegis Systems more rapidly, but they will also consume more Aegis Cells, and there is a limit to the rate at which any Aegis System can be Disrupted."
                ));

        //TODO: the Aegis Hub and Aegis Interference

        GlossarInit.addCategory(cat);
    }

    public void updateRuleConfigValues(FileConfiguration config, boolean writeToFile){
        DISRUPTION_HP_PER_STRUCTURE_POINT = config.getConfigurableFloat("DISRUPTION_HP_PER_STRUCTURE_POINT",1.0f);
        DISRUPTION_VULNERABILITY_WINDOW_DURATION_BASE = MS_TO_MINUTES * config.getConfigurableInt("DISRUPTION_VULNERABILITY_WINDOW_MINUTES",45); //45min
        MINIMUM_DISRUPTION_PREPARATION_TIME_MS = (long) (MS_TO_HOURS * config.getConfigurableFloat("",6)); //6hr
        BASTION_FIELD_PROTECTS_TURRETS = config.getConfigurableBoolean("BASTION_FIELD_PROTECTS_TURRETS",false);
        AEGIS_FTL_TRAP_ALLOWED = config.getConfigurableBoolean("FTL_TRAP_SUBSYS_ENABLED",true);
        AEGIS_FTL_WALL_ALLOWED = config.getConfigurableBoolean("FTL_REPEL_SUBSYS_ENABLED", true);
        AEGIS_FTL_CONTROL_FACTION_CLAIM_REQUIRED = config.getConfigurableBoolean("FTL_CONTROL_SUBSYSTEM_REQUIRES_FACTION_CLAIM", false);
    }
}

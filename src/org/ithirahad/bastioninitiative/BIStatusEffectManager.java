package org.ithirahad.bastioninitiative;

import api.listener.events.register.RegisterConfigGroupsEvent;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.data.blockeffects.config.ConfigGroup;
import org.schema.game.common.data.blockeffects.config.EffectConfigElement;
import org.schema.game.common.data.blockeffects.config.elements.ModifierStackType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectFloatValue;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectParameterType;
import org.schema.game.common.data.blockeffects.config.parameter.StatusEffectWeaponType;
import org.schema.game.server.data.GameServerState;

import static org.ithirahad.bastioninitiative.BIConfiguration.BASTION_SHIELD_AMPLIFICATION_FACTOR;
import static org.ithirahad.bastioninitiative.BIConfiguration.BASTION_WEAPON_RANGE_FACTOR;
import static org.schema.game.common.data.blockeffects.config.StatusEffectType.*;

public class BIStatusEffectManager {
    public static final String bastionShieldAmplification = "bastion_shield_amp";
    public static final String bastionWeaponEnhancement = "bastion_weapon_amp";
    public static final ConfigGroup bastionFieldStatusEffect = new ConfigGroup("bastion_field_protection"){
        @Override
        public String getEffectDescription() {
            return "Protects the structure and any docked entities from all damage.\r\nDoes not protect active turrets.";
        }
    };
    //Grants limited invincibility via defense listener, and makes station vulnerable to disruption if not in vulnerability period.
    public static final ConfigGroup aegisInterference = new ConfigGroup("aegis_interference"){
        @Override
        public String getEffectDescription() {
            return "Something is causing interference with your Aegis Core! Aegis Subsystems may not function until this is resolved.";
        }
    };
    //Another placeholder. Prevents Bastion Field protection from working. Not sure what the mechanics of this should be.
    //TODO: lightning VFX for this. Needs a good visual indicator.
    //ex. if someone tries to set up two principal claim stations in a system at once, they'd simply be unprotected

    //TODO: expandable dictionary for addon/extension mods (e.g. fuel consumption reduction)? Depends on custom effect types though
    public static void createEffects(RegisterConfigGroupsEvent event){

        //BASTION SHIELD AMPLIFICATION
        ConfigGroup bastionShields = new ConfigGroup(bastionShieldAmplification);
        final EffectConfigElement shieldBuff = new EffectConfigElement();
        final EffectConfigElement shieldUpkeepComp = new EffectConfigElement();

        shieldBuff.init(SHIELD_CAPACITY); //magical mystery function that, among other things, sets the type. (why isn't this in the constructor?)
        //note for reference: there are a bunch of StatusEffectParameter types;
        // in this case (shield capacity) the effect float value is the relevant one.
        // this is why we can't just say shieldBuff.value.value.set(...);
        shieldBuff.stackType = ModifierStackType.MULT;
        StatusEffectFloatValue shieldBuffValue = new StatusEffectFloatValue();
        shieldBuffValue.value.set(BASTION_SHIELD_AMPLIFICATION_FACTOR);
        shieldBuff.value = shieldBuffValue;
        // The word "value" is beginning to lose all value and meaning.
        //
        // Help.
        //
        // ...another note for reference: by just dynamically changing the value inside the floatmodifiers for stuff and resynching,
        // we could have PROPER effect passives back! >:(

        shieldUpkeepComp.stackType = ModifierStackType.MULT;
        shieldUpkeepComp.init(SHIELD_CAPACITY_UPKEEP);
        StatusEffectFloatValue shieldCompValue = new StatusEffectFloatValue();
        shieldCompValue.value.set(1 / BASTION_SHIELD_AMPLIFICATION_FACTOR);
        //TODO: figure out the actual maths for this. probably need the upkeep value itself from config
        shieldUpkeepComp.value = shieldCompValue;

        bastionShields.elements.add(shieldBuff);
        bastionShields.elements.add(shieldUpkeepComp);
        //TODO: Is the ConfigPool the only thing needed?

        event.getModConfigGroups().enqueue(bastionShields);

        //------------------------------------------------------------------------------------------------------------

        //BASTION WEAPON RANGE AMPLIFICATION
        ConfigGroup bastionWeapons = new ConfigGroup(bastionWeaponEnhancement);
        bastionWeapons.elements.add(createRangeBuff(DamageDealerType.BEAM));
        bastionWeapons.elements.add(createRangeBuff(DamageDealerType.PROJECTILE));
        bastionWeapons.elements.add(createRangeBuff(DamageDealerType.MISSILE));
        event.getModConfigGroups().enqueue(bastionWeapons);

        //------------------------------------------------------------------------------------------------------------
        //can't add damage-taken modifier to the bastion field unfortunately, as it applies before shields!
        event.getModConfigGroups().enqueue(bastionFieldStatusEffect);
        event.getModConfigGroups().enqueue(aegisInterference);
    }

    private static EffectConfigElement createRangeBuff(DamageDealerType damageDealerType) {
        final EffectConfigElement rangeBuff = new EffectConfigElement();
        rangeBuff.init(WEAPON_RANGE);
        MiscUtils.setPrivateField("type",rangeBuff.weaponType,StatusEffectParameterType.WEAPON_TYPE); //this is final and set wrong
        ((StatusEffectWeaponType)rangeBuff.weaponType).value.set(damageDealerType.ordinal());
        rangeBuff.stackType = ModifierStackType.MULT;
        rangeBuff.priority = 0;
        StatusEffectFloatValue rangeBuffValue = new StatusEffectFloatValue();
        rangeBuffValue.value.set(BASTION_WEAPON_RANGE_FACTOR);
        rangeBuff.value = rangeBuffValue;
        return rangeBuff;
    }
}

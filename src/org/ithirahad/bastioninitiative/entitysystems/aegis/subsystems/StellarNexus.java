package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.BISovereigntyConfiguration;
import org.ithirahad.bastioninitiative.entitysystems.aegis.PersistentAegisSubsystem;
import org.ithirahad.bastioninitiative.persistence.FactionSystemControlSheet;
import org.ithirahad.bastioninitiative.persistence.SystemControlInfo;
import org.ithirahad.bastioninitiative.persistence.VirtualStellarNexus;
import org.schema.common.util.StringTools;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.schine.graphicsengine.core.Timer;

import javax.annotation.CheckForNull;
import java.util.LinkedList;
import java.util.List;

import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.sovContainer;

public class StellarNexus extends PersistentAegisSubsystem<VirtualStellarNexus> {
    private boolean sheetUpdateNeeded = true;

    public StellarNexus(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, "Aegis Stellar Nexus", VirtualStellarNexus.class);
    }

    @Override
    public void onSerialize(PacketWriteBuffer b){
    }

    @Override
    public void onDeserialize(PacketReadBuffer b){
    }

    @Override
    protected void initialize() {
        super.initialize();
        sheetUpdateNeeded = true;
    }

    public boolean isProtecting(){
        return virtualSystem != null && !virtualSystem.linked.isEmpty() && virtualSystem.isOperating(false);
    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        int result = STELLAR_NEXUS_FUEL_BASE;
        result += (int) (virtualSystem.getNetworkSize() * STELLAR_NEXUS_FUEL_PER_LINKED_STRUCTURE);
        result += (int) (virtualSystem.getNetworkTotalStructurePoints() * STELLAR_NEXUS_FUEL_PER_LINKED_CONSUMPTION);
        result += (int) (getEnhancement() * STELLAR_NEXUS_FUEL_PER_ENHANCER);
        return result;
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return STELLAR_NEXUS_ONLINING_COST;
    }

    @Override
    public double getPowerConsumptionActive() {
        return STELLAR_NEXUS_POWER_CONSUMPTION;
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Stellar Nexus Subsystem";
    }

    @CheckForNull
    @Override
    public List<String> getInfoLines() {
        int linked = virtualSystem.getNetworkSize();
        int linking = virtualSystem.getAmountQueued();
        long now = System.currentTimeMillis();
        List<String> result = new LinkedList<>();
        float discountPercent = (100f-(100f*virtualSystem.getChargeCostFactor()));
        discountPercent = ((float)Math.round(discountPercent * 100))/100;
        result.add("Charge cost reduction for linked structures (from enhancers): " + discountPercent + '%');
        result.add("Linked: " + linked + " Structure" + (linked == 1?"":'s')); //the pluralizing ternary is an ugly mf lmao
        if(virtualSystem.getDisturbedUntil() > now){
            long diff = virtualSystem.getDisturbedUntil() - now;
            result.add("Linked Structure Was Disrupted:");
            result.add("Cannot link new structures for " + StringTools.formatTimeFromMS(diff));
        }
        if(linking > 0) result.add("Currently Linking " + linking + " Structure" + (linking == 1?"":'s'));
        if(linked > 0){
            result.add("Faction influence from structures:");
            result.add("Base: " + BISovereigntyConfiguration.INFLUENCE_BASE_FROM_STELLAR_NEXUS);
            result.add("From Linked Structures: " + virtualSystem.getNetworkTotalStructurePoints() * BISovereigntyConfiguration.INFLUENCE_FROM_STELLAR_NEXUS_PER_LINKED_STRUCT_POINT);
        } else result.add("Faction influence from structures: 0");
        return result;
    }

    @Override
    protected void handleServer(Timer timer, boolean hasBlocks) {
        super.handleServer(timer,hasBlocks);
        if(isInitialized()) {
            SegmentController s = getParentCore().segmentController;
            if (s.isFullyLoaded() && !s.isMarkedForDeleteVolatile()) {
                SystemControlInfo controlInfo = sovContainer.getOrCreateInfo(virtualSystem.getParent().getStarSystemLocation());
                FactionSystemControlSheet sheet = controlInfo.getOrAddFactionSheet(virtualSystem.getFactionID());
                String uid = virtualSystem.getParent().uid;
                sheetUpdateNeeded = !uid.equals(sheet.getNexusUID());
                if(sheetUpdateNeeded) {
                    sheet.setNexusUID(uid);
                    //it should not be possible to reach here without a faction profile being present from the station, but here we are.
                    sheetUpdateNeeded = false;
                }
                virtualSystem.setEnhanced(getEnhancement());
            }
            if (!s.isHomeBase()) {
                if (isProtecting()) s.setVulnerable(false);
                else s.setVulnerable(true);
            }
        }
        //TODO if more than one in faction in system, apply aegis interference to everything!
        // Once there is more than one interference source, we're going to need a consensus system to avoid constantly adding/stripping interference for different reasons.
    }

    @Override
    public String getShortPurposeString() {
        return "* Links to other Aegis Cores in the system, providing them with reduced Aegis Charge costs based on this system's enhancement level." +
                "\r\n* Shields this structure from all damage, so long as at least one station remains linked." +
                "\r\n* Grants your faction bonus influence based on the structure points of linked stations.";
    }
}

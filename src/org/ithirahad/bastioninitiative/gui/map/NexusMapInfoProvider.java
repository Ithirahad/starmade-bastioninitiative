package org.ithirahad.bastioninitiative.gui.map;

import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.network.NexusMapPacket;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.persistence.VirtualStellarNexus;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;

/**
 * Provides information to the client regarding the nexus layout of a given system.
 */
public class NexusMapInfoProvider {
    public static void handle(Vector3i systemLocation, PlayerState playerState) {
        ArrayList<NexusTree> trees = new ArrayList<>();
        HashSet<VirtualAegisSystem> systemsHere = bsiContainer.getAegisSystemsInSystem(systemLocation);

        if(systemsHere != null) for(VirtualAegisSystem sys : systemsHere){
            if(sys != null && sys.hasSubsystem("Aegis Stellar Nexus") && playerCanSee(playerState,systemLocation,sys.getFactionID())){ //shouldn't be, but judging what should and should not happen isn't this class's job
                VirtualStellarNexus nexus = (VirtualStellarNexus)sys.getSubsystem("Aegis Stellar Nexus");
                nexus.update();
                if(nexus.isOperating(false)){
                    int ownFaction = playerState.getFactionId();
                    Set<Vector3i> linkedLocs = new HashSet<>();
                    Set<Vector3i> linkingLocs = new HashSet<>();

                    for(String uid : nexus.linked){
                        linkedLocs.add(bsiContainer.getAegisSystemByID(uid).getSectorLocation());
                    }

                    for(String uid : nexus.linking.keySet()){
                        linkingLocs.add(bsiContainer.getAegisSystemByID(uid).getSectorLocation());
                    }

                    NexusTree tree = new NexusTree(
                            NexusTree.FactionRelationship.fromIds(ownFaction,nexus.getFactionID()),
                            nexus.getParent().getSectorLocation(),
                            linkedLocs,
                            linkingLocs
                    );

                    trees.add(tree);
                }
            }
        }

        NexusMapPacket toSend = new NexusMapPacket(trees);
        PacketUtil.sendPacket(playerState,toSend);
    }

    private static boolean playerCanSee(PlayerState player, Vector3i system, int factionID) {
        return factionID == player.getFactionId() || player.getFogOfWar().isVisibleSystemClient(system);
    }
}

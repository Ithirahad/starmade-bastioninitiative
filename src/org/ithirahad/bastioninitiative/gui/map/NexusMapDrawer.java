package org.ithirahad.bastioninitiative.gui.map;

import api.listener.fastevents.GameMapDrawListener;
import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.network.NexusMapRequest;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gamemap.GameMapDrawer;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.LinkedList;
import java.util.List;

import static org.ithirahad.bastioninitiative.gui.map.NexusTree.FactionRelationship.*;

public class NexusMapDrawer implements GameMapDrawListener {
    private static final float ADDITIVE_ADJUSTMENT = 3.125f;
    private static final Tuple3f addAdj = new Vector3f();
    private static final float MULT_ADJUSTMENT = 6.25f;
    private static final float LINKED_LINE_THICKNESS = 3.0f;
    private static final float LINKING_LINE_THICKNESS = 1.25f;
    private static final long INFO_REFRESH_FROM_SERVER_INTERVAL_MS = 2000;
    private static List<NexusTree> nexusTreeList = new LinkedList<>();
    private static final float alpha = 0.3f;
    private static final Vector3i lastSystem = new Vector3i();
    private static long lastRefreshFromServer = 0;

    public NexusMapDrawer(){
        addAdj.set(ADDITIVE_ADJUSTMENT,ADDITIVE_ADJUSTMENT,ADDITIVE_ADJUSTMENT);
    }

    public static void addNexusTrees(Iterable<NexusTree> treeList) {
        nexusTreeList.clear();
        for(NexusTree tree : treeList)
            nexusTreeList.add(tree);
    }

    @Override
    public void system_PreDraw(GameMapDrawer gameMapDrawer, Vector3i system, boolean b) {
        long now = System.currentTimeMillis();
        if(!system.equals(lastSystem) || now - lastRefreshFromServer > INFO_REFRESH_FROM_SERVER_INTERVAL_MS){
            NexusMapRequest r = new NexusMapRequest(system);
            PacketUtil.sendPacketToServer(r);
            lastRefreshFromServer = now;
            lastSystem.set(system);
        }
    }

    @Override
    public void system_PostDraw(GameMapDrawer gameMapDrawer, Vector3i vector3i, boolean b) {

    }

    @Override
    public void galaxy_PreDraw(GameMapDrawer gameMapDrawer) {

    }

    @Override
    public void galaxy_PostDraw(GameMapDrawer gameMapDrawer) {

    }

    @Override
    public void galaxy_DrawLines(GameMapDrawer gameMapDrawer) {
        final Vector3f start = new Vector3f();
        final Vector3f end = new Vector3f();
        Vector4f color1;
        Vector4f color2;
        for(NexusTree t : nexusTreeList) {
            start.set(t.root.toVector3f());
            start.scale(MULT_ADJUSTMENT);
            start.add(addAdj);

            color1 = new Vector4f();
            if(t.factionRelation == SELF) color1.set(0.07f,1f,0.35f, alpha);
            else if(t.factionRelation == ALLY) color1.set(0.5f,1f,0.75f, alpha);
            else if(t.factionRelation == ENEMY) color1.set(1f,0,0,alpha);
            else color1.set(0.5f,1,0.8f,alpha*0.75f);
            color2 = new Vector4f(color1);
            color2.w *= (float) (0.4 + (0.6*Math.sin(((double)System.currentTimeMillis()/200d)))); //fade in and out
            color2.w *= 0.7f;

            for(Vector3i linked : t.linked){
                end.set(linked.toVector3f());
                end.scale(MULT_ADJUSTMENT);
                end.add(addAdj);

                DrawUtils.drawFTLLine(start,end,color1,color1);
            }

            for(Vector3i linking : t.linking){
                end.set(linking.toVector3f());
                end.scale(MULT_ADJUSTMENT);
                end.add(addAdj);

                DrawUtils.drawFTLLine(start,end,color1,color2);
            }
        }
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer gameMapDrawer) {

    }

    @Override
    public void galaxy_DrawQuads(GameMapDrawer gameMapDrawer) {

    }
}

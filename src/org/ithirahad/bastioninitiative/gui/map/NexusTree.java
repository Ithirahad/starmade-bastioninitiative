package org.ithirahad.bastioninitiative.gui.map;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.GameServerState;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class NexusTree implements Serializable {
    public final FactionRelationship factionRelation;
    public final Vector3i root;
    public final ArrayList<Vector3i> linked;
    public final ArrayList<Vector3i> linking;

    public NexusTree(FactionRelationship rel, Vector3i root, Collection<Vector3i> attached, Collection<Vector3i> inProgress){
        factionRelation = rel;
        this.root = root;
        linked = new ArrayList<>(attached);
        linking = new ArrayList<>(inProgress);
    }

    public enum FactionRelationship {
        SELF,
        ALLY,
        NEUTRAL,
        ENEMY,
        UNKNOWN; //error state

        public static FactionRelationship fromIds(int ownFaction, int factionID) {
            if(factionID == ownFaction) return SELF;
            else {
                FactionManager facman = GameServerState.instance.getFactionManager();
                FactionRelation.RType rel = facman.getRelation(ownFaction,factionID);
                switch(rel){
                    case FRIEND: return ALLY;
                    case NEUTRAL: return NEUTRAL;
                    case ENEMY: return ENEMY;
                }
            }
            return UNKNOWN;
        }
    }
}

package org.ithirahad.bastioninitiative.gui.corepanel.elements;

import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIProgressBarDynamic;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector4f;

import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_DAYS;

@NotImplemented
public class AegisRecalibrationElement extends GUIProgressBarDynamic {
	final static Vector4f PLACEHOLDER_PURPLE_STARTING_COLOUR = new Vector4f(0.7f,0.1f,1f,1f);
	final static Vector4f PLACEHOLDER_PINK_ENDING_COLOUR = new Vector4f(1f,0.64f,0.8f,1f);

	private VirtualAegisSystem.AegisSystemStats stats;
	private VirtualAegisSystem.AegisSystemStatus status;

	public AegisRecalibrationElement(InputState inputState, int width, int height, VirtualAegisSystem.AegisSystemStats stats, VirtualAegisSystem.AegisSystemStatus status) {
		super(inputState, width, height);
		this.stats = stats;
		this.status = status;
		onInit();
		setEmptyColor(PLACEHOLDER_PURPLE_STARTING_COLOUR);
		setFilledColor(PLACEHOLDER_PINK_ENDING_COLOUR);
		setBackgroundColor(new Vector4f(0.15f,0.15f,0.18f,1f));
	}

	@Override
	public FontLibrary.FontSize getFontSize() {
		return FontLibrary.FontSize.MEDIUM;
	}

	@Override
	public String getLabelText() {
		return "GUH";
	}

	@Override
	public float getValue() {
		return (float)(stats.getRecalibrationStart()/(double)MS_TO_DAYS);
	}
}

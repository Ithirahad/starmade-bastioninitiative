package org.ithirahad.bastioninitiative.gui.flighthud;

import org.schema.common.util.linAlg.Vector4i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gui.shiphud.newhud.FillableHorizontalBar;
import org.schema.game.client.view.gui.shiphud.newhud.GUIPosition;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;

import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.setSuperclassPrivateField;


public class DisruptionProgressHUDBar extends FillableHorizontalBar {
    private static final Vector4i COLOR_PLACEHOLDER = new Vector4i(255,150,190,255); //obnoxious colour we should see; means something went wrong
    private static final Vector4f COLOR_ACTIVE = new Vector4f(0.666f,1f,0f,1f);//green machine
    // ...Should this be light red instead for contrast and to avoid looking like an HP bar?
    private static final Vector4f COLOR_INACTIVE = new Vector4f(0.75f,0.75f,0.785f,1f); //greyish
    private static final long TIMEOUT_MS = 5000;
    private static final GUIPosition CENTERING_CENTERED = new GUIPosition();
    private static final Vector2f OFFSET = new Vector2f(0f, 100f);
    private static final Vector2f TEXT_POS = new Vector2f(0, 25);
    private static final Vector2f TEXT_DESCRIPTION_POS = new Vector2f(0, 50);
    boolean disruptable = false;
    long lastUpdate = 0L;
    float hp = 1;
    float maxHp = 2;

    public DisruptionProgressHUDBar(InputState inputState) {
        super(inputState);
        int centered = 0b0;
        centered |= ORIENTATION_VERTICAL_MIDDLE;
        centered |= ORIENTATION_HORIZONTAL_MIDDLE;
        CENTERING_CENTERED.value = centered;
        // why couldn't they be normal and just use two attributes for horiz. and vert. align, rather than a magic format with bit masking of all things?
        /*
        float x = GLFrame.getWidth()*0.5f;
        float y = GLFrame.getHeight()*0.5f;
        y += 60f; //get it below the central circle thingie somewhere
        OFFSET.set(x,y);
         */
    }

    public void updateInfo(boolean canBeDisrupted, double currentHP, double maxHP) {
        disruptable = canBeDisrupted;
        this.hp = (float) currentHP;
        this.maxHp = (float) maxHP;
        lastUpdate = now();
    }

    @Override
    public float getFilled() {
        return 1 - (hp/maxHp); //this is a progress bar for hacking, not an HP bar, so it is inverted, treating the HP loss as progress.
    }

    @Override
    public Vector4i getConfigColor() {
        return COLOR_PLACEHOLDER;
    }

    @Override
    public GUIPosition getConfigPosition() {
        return CENTERING_CENTERED;
    }

    @Override
    public Vector2f getConfigOffset() {
        return OFFSET;
        //If this is absolute, this should centre it. otherwise 0.5,0.5 should do the trick...?
    }

    @Override
    public boolean isPositionCenter() {
        return true;
    }

    @Override
    protected String getTag() {
        return "BastionInitiativeHackingProgressBar";
    }

    @Override
    public boolean isBarFlippedX() {
        return false;
    }

    @Override
    public boolean isBarFlippedY() {
        return false;
    }

    @Override
    public boolean isFillStatusTextOnTop() {
        return false; //it would get in the way
    }

    @Override
    public Vector2f getTextPos() {
        return TEXT_POS;
    }

    @Override
    public Vector2f getTextDescPos() {
        return TEXT_DESCRIPTION_POS;
    }

    @Override
    public void draw() {
        if(isNotTimedOut() && !GameClientState.instance.getPlayer().getControllerState().getUnits().isEmpty()){
            updateOrientation();
            setSuperclassPrivateField(FillableHorizontalBar.class,"color",this,disruptable ? COLOR_ACTIVE : COLOR_INACTIVE);
            //why tf is the colour protected so tightly anyway lmao
            //update shader params?
            super.draw();
        }
    }

    @Override
    public String getText() {
        if(isNotTimedOut()){
            if(disruptable) return "Disrupted: " + (maxHp - hp) + '/' + maxHp; //TODO: Display disruption rate
            else return "Cannot Disrupt This Structure!";
        }
        else return "";
    }

    private boolean isNotTimedOut(){
        return now() - lastUpdate < TIMEOUT_MS;
    }
}

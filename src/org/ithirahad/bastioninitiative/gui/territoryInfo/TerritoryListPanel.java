package org.ithirahad.bastioninitiative.gui.territoryInfo;

import api.utils.gui.GUIMenuPanel;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.input.InputState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class TerritoryListPanel extends GUIMenuPanel {
    TerritoryScrollableList territoryList;
    GUIContentPane contentTabPane;
    private Set<TerritoryControlOverview> content = Collections.emptySet();
    private GUIElementList listInternals;

    public TerritoryListPanel(InputState state) {
        super(state, "TerritoryListPanel", 800, 500);
    }

    @Override
    public void onInit() {
        super.onInit();
        guiWindow.orientate(ORIENTATION_HORIZONTAL_MIDDLE | ORIENTATION_VERTICAL_MIDDLE);
    }

    @Override
    public void recreateTabs() {
        guiWindow.activeInterface = this;

        if(!guiWindow.getTabs().isEmpty()) guiWindow.clearTabs();
        contentTabPane = guiWindow.addTab("TERRITORY");
        territoryList = new TerritoryScrollableList(this.getState(), contentTabPane.getContent(0));
        territoryList.onInit();
        contentTabPane.getContent(0).attach(territoryList);
        //TODO: second column to display selection;
        // corresponding callback action to switch displayed info here, incl. RRS system type
        listInternals = new GUIElementList(getState());
        territoryList.updateListEntries(listInternals,content);
    }

    public void updateInfo(Set<TerritoryControlOverview> newInfo) {
        content = newInfo;
        recreateTabs();
    }
}

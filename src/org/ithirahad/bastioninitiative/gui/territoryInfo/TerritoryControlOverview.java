package org.ithirahad.bastioninitiative.gui.territoryInfo;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import org.ithirahad.bastioninitiative.persistence.SystemControlInfo;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import static api.mod.ModStarter.justStartedServer;

public class TerritoryControlOverview {
    public final Vector3i location;
    public int neutralControl;
    public final Int2IntOpenHashMap controlLevels = new Int2IntOpenHashMap();
    public int ownerFaction;
    private final String systemName;

    public TerritoryControlOverview(SystemControlInfo source) {
        if(!justStartedServer) throw new IllegalStateException("[BastionInitiative][ERROR] Cannot construct TerritoryControlOverview on dedicated client!");
        location = new Vector3i(source.getLocation());
        neutralControl = source.getNeutralInfluence();
        for(Integer faction : source.getPresentFactions()){
            controlLevels.add(faction,source.getInfluence(faction));
        }
        ownerFaction = source.getControllingFactionID();
        Galaxy galaxy = MiscUtils.getGalaxyFromSystemPosSafe(GameServerState.instance.getUniverse(),location);
        systemName = galaxy.getName(Galaxy.getRelPosInGalaxyFromAbsSystem(location, new Vector3i()));
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TerritoryControlOverview){
            return ((TerritoryControlOverview)obj).location.equals(location);
        }
        return super.equals(obj);
    }

    public String getSystemName() {
        return systemName;
    }

    //TODO: Add per-faction infos regarding the source of influence, also separate neutral influence factors e.g. from base, from stations, etc
}

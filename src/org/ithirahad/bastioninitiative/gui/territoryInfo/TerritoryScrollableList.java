package org.ithirahad.bastioninitiative.gui.territoryInfo;

import api.common.GameClient;
import org.hsqldb.lib.StringComparator;
import org.ithirahad.bastioninitiative.network.TerritoryInfoPacket;
import org.ithirahad.bastioninitiative.persistence.SystemControlPersistenceContainer;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

public class TerritoryScrollableList extends ScrollableTableList<TerritoryControlOverview> {
    private Set<TerritoryControlOverview> source;

    public TerritoryScrollableList(InputState state, GUIElement p) {
        super(state, 100, 100, p);
    }

    private class SystemRow extends Row {
        public SystemRow(InputState state, TerritoryControlOverview info, GUIElement... elements) {
            super(state, info, elements);
            this.highlightSelect = true;
        }
    }

    @Override
    public void initColumns() {
        addColumn(Lng.str("Name"), 25, new Comparator<TerritoryControlOverview>() {
            @Override
            public int compare(TerritoryControlOverview o1, TerritoryControlOverview o2) {
                return o1.getSystemName().compareToIgnoreCase(o2.getSystemName());
            }
        });
        addColumn(Lng.str("Location"), 8, new Comparator<TerritoryControlOverview>() {
            @Override
            public int compare(TerritoryControlOverview o1, TerritoryControlOverview o2) {
                Faction us = GameClientState.instance.getFaction();
                Vector3i ref; //faction homebase if faction has one, else player system
                if(us.getHomebaseUID().isEmpty()) ref = GameClientState.instance.getCurrentClientSystem().getPos();
                else ref = us.getHomeSector();

                long d2First = Vector3i.getDisatanceSquaredD(ref,o1.location);
                long d2Second = Vector3i.getDisatanceSquaredD(ref,o2.location);
                if(d2First == d2Second) return 0;
                else return d2First < d2Second ? 1 : -1; //closer to HB/player loc = higher in sorting
            }
        });
        addColumn(Lng.str("Owner"), 28, new Comparator<TerritoryControlOverview>() {
            @Override
            public int compare(TerritoryControlOverview o1, TerritoryControlOverview o2) {
                int self = GameClientState.instance.getFaction().getIdFaction();
                if(o1.ownerFaction == self){
                    if(o2.ownerFaction == self) return 0;
                    else return 1;
                }
                else if(o2.ownerFaction == self) return -1;
                //sort self first

                return Integer.compare(o1.ownerFaction,o2.ownerFaction);
            }
        });
        addColumn(Lng.str("Owner Influence"), 10, new Comparator<TerritoryControlOverview>() {
            @Override
            public int compare(TerritoryControlOverview o1, TerritoryControlOverview o2) {
                int crtl1 = o1.ownerFaction == 0? o1.neutralControl : o1.controlLevels.get(o1.ownerFaction);
                int crtl2 = o2.ownerFaction == 0? o2.neutralControl : o2.controlLevels.get(o2.ownerFaction);
                return Integer.compare(crtl1,crtl2);
            }
        });
        addColumn(Lng.str("Your Influence"), 10, new Comparator<TerritoryControlOverview>() {
            @Override
            public int compare(TerritoryControlOverview o1, TerritoryControlOverview o2) {
                int self = GameClientState.instance.getFaction().getIdFaction();
                return Integer.compare(o1.controlLevels.get(self),o2.controlLevels.get(self));
            }
        });
    }

    @Override
    protected Collection<TerritoryControlOverview> getElementList() {
        return source;
    }

    @Override
    public void updateListEntries(GUIElementList guiElementList, Set<TerritoryControlOverview> set) {
        source = set;
        for(final TerritoryControlOverview info : set){
            //initialize GUI text pieces
            GUITextOverlayTable nameText = new GUITextOverlayTable(10, 10, getState());
            nameText.setTextSimple(tsobj(info.getSystemName()));
            GUITextOverlayTable locationText = new GUITextOverlayTable(10, 10, getState());
            locationText.setTextSimple(info.location);
            GUITextOverlayTable ownerText = new GUITextOverlayTable(10, 10, getState()) {
                @Override
                public void draw() {
                    FactionRelation.RType relation = fmg().getRelation(player().getName(), player().getFactionId(), info.ownerFaction);
                    setColor(org.schema.game.client.view.gui.shiphud.newhud.ColorPalette.getColorDefault(relation, info.ownerFaction == player().getFactionId()));
                    super.draw();
                }
            };
            ownerText.setTextSimple(tsobj(fmg().getFactionName(info.ownerFaction)));

            GUITextOverlayTable ownerInfluenceText = new GUITextOverlayTable(10, 10, getState());
            ownerInfluenceText.setTextSimple(info.ownerFaction == 0? (Integer)info.neutralControl:(Integer)info.controlLevels.get(info.ownerFaction));

            GUITextOverlayTable selfInfluenceText = new GUITextOverlayTable(10, 10, getState());
            selfInfluenceText.setTextSimple((Integer)info.controlLevels.get(player().getFactionId()));

            //Create a row for this system
            final SystemRow row = new SystemRow(getState(),info,nameText,locationText,ownerText,ownerInfluenceText,selfInfluenceText);
            row.expanded = new GUIElementList(getState());
            row.onInit();
            guiElementList.addWithoutUpdate(row);
        }
    }

    private static Object tsobj(final String output) {
        return new Object() {
            @Override
            public String toString() {
                return output;
            }
        };
    }

    private FactionManager fmg(){
        return ((GameClientState) getState()).getFactionManager();
    }

    private PlayerState player(){
        return ((GameClientState) getState()).getPlayer();
    }
}

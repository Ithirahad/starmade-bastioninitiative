package org.ithirahad.bastioninitiative.gui.territoryInfo;

import api.utils.gui.GUIControlManager;
import api.utils.gui.GUIMenuPanel;
import org.schema.game.client.data.GameClientState;

import java.util.ArrayList;
import java.util.HashSet;

import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.territoryInfoPanel;

public class TerritoryListControlManager extends GUIControlManager {
    public TerritoryListControlManager(GameClientState clientState) {
        super(clientState);
    }

    public void updateInfo(ArrayList<TerritoryControlOverview> content) {
        territoryInfoPanel.updateInfo(new HashSet<TerritoryControlOverview>(content));
    }

    @Override
    public GUIMenuPanel createMenuPanel() {
        return territoryInfoPanel;
    }

    @Override
    public GUIMenuPanel getMenuPanel() {
        return territoryInfoPanel;
    }
}

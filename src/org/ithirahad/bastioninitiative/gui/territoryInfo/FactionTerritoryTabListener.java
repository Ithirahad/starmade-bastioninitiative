package org.ithirahad.bastioninitiative.gui.territoryInfo;

import api.listener.Listener;
import api.listener.events.gui.MainWindowTabAddEvent;
import api.network.packets.PacketUtil;
import api.utils.StarRunnable;
import org.ithirahad.bastioninitiative.network.TerritoryInfoRequest;
import org.schema.game.client.view.gui.faction.newfaction.FactionOptionFactionContent;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalArea;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIHorizontalButtonTablePane;
import org.schema.schine.input.InputState;

import static org.ithirahad.bastioninitiative.BastionInitiative.modInstance;
import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.territoryInfoControlManager;
import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.territoryInfoPanel;

public class FactionTerritoryTabListener extends Listener<MainWindowTabAddEvent> {
    public static GUIContentPane optionsPane;
        @Override
        public void onEvent(MainWindowTabAddEvent event) {
            if(event.getTitle().equals(Lng.str("OPTIONS"))){
                optionsPane = event.getPane();
                new StarRunnable(){
                    @Override
                    public void run() {
                        modifyPane();
                        this.cancel();
                    }
                }.runLater(modInstance,1);
            }
        }

        public static void modifyPane(){
            final FactionOptionFactionContent facc = (FactionOptionFactionContent) optionsPane.getTextboxes(0).get(1).getContent();
            GUIElementList node = (GUIElementList) facc.getChilds().get(0); //probably a container...?
            GUIHorizontalButtonTablePane facbutts = (GUIHorizontalButtonTablePane) node.get(0).getContent(); //The other children are selection buttons for faction options. Potentially useful?
            //TODO: automatically find next x/y index in facbutts and increment.
            // Some other mod might add a button or buttons to this also.
            facbutts.addButton(1, 4, Lng.str("View Territory Info"), GUIHorizontalArea.HButtonType.BUTTON_BLUE_MEDIUM, new GUICallback() {
                        @Override
                        public void callback(GUIElement guiElement, MouseEvent event) {
                            if (event.pressedLeftMouse()) {
                                territoryInfoControlManager.onInit();
                                territoryInfoPanel.onInit();
                                territoryInfoControlManager.setActive(true);
                                PacketUtil.sendPacketToServer(new TerritoryInfoRequest());
                            }
                        }

                        @Override
                        public boolean isOccluded() {
                            return !facc.isActive();
                        }
                    },
                    new GUIActivationCallback() {
                        @Override
                        public boolean isVisible(InputState inputState) {
                            return true;
                        }

                        @Override
                        public boolean isActive(InputState inputState) {
                            return true;
                        }
                }
            );
        }
}

package org.ithirahad.bastioninitiative.gui;

import org.ithirahad.bastioninitiative.gui.corepanel.AegisSystemManagementControlManager;
import org.ithirahad.bastioninitiative.gui.corepanel.AegisSystemManagementPanel;
import org.ithirahad.bastioninitiative.gui.corepanel.RecalibrationTimeInput;
import org.ithirahad.bastioninitiative.gui.flighthud.DisruptionProgressHUDBar;
import org.ithirahad.bastioninitiative.gui.old.AegisSystemSimpleInput;
import org.ithirahad.bastioninitiative.gui.territoryInfo.TerritoryListControlManager;
import org.ithirahad.bastioninitiative.gui.territoryInfo.TerritoryListPanel;

public class BIGUIInstanceContainer { //BI GUI, not Big UI! :P
    public static AegisSystemSimpleInput temporaryAegisSystemDialog;

    public static DisruptionProgressHUDBar disruptionDatalinkBarHUD;

    public static AegisSystemManagementPanel aegisSystemPanel;
    public static AegisSystemManagementControlManager aegisSystemUIControlManager;

    public static RecalibrationTimeInput recalibrationTimeDialog;

    public static TerritoryListPanel territoryInfoPanel;
    public static TerritoryListControlManager territoryInfoControlManager;
}

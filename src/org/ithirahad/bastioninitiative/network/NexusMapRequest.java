package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.gui.map.NexusMapInfoProvider;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class NexusMapRequest extends Packet {
    Vector3i systemCoord;
    public NexusMapRequest(){

    }

    public NexusMapRequest(Vector3i sys){
        systemCoord = sys;
    }
    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        this.systemCoord = b.readVector();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeVector(systemCoord);
    }

    @Override
    public void processPacketOnClient() {
        //no.
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        NexusMapInfoProvider.handle(systemCoord,playerState);
    }
}

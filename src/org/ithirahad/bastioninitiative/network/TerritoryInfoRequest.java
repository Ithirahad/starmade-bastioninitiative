package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class TerritoryInfoRequest extends Packet {
    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {

    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {

    }

    @Override
    public void processPacketOnClient() {
        //no
    }

    @Override
    public void processPacketOnServer(PlayerState player) {
        if(player.getFactionId() != 0){
            PacketUtil.sendPacket(player, new TerritoryInfoPacket(player.getFactionId()));
        }
    }
}

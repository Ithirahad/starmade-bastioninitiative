package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.gui.map.NexusMapDrawer;
import org.ithirahad.bastioninitiative.gui.map.NexusTree;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;
import java.util.ArrayList;

public class NexusMapPacket extends Packet {
    ArrayList<NexusTree> content = new ArrayList<>();
    private int size;

    public NexusMapPacket(){

    }

    public NexusMapPacket(ArrayList<NexusTree> trees){
        this.content = trees;
    }
    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        content = new ArrayList<NexusTree>();
        size = b.readInt();
        for(int i = 0; i < size; i++) {
            NexusTree nt = b.readObject(NexusTree.class);
            content.add(nt);
        }
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        size = content.size();
        b.writeInt(size);
        for(NexusTree n : content) b.writeObject(n);
    }

    @Override
    public void processPacketOnClient() {
        NexusMapDrawer.addNexusTrees(content);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //no.
    }
}

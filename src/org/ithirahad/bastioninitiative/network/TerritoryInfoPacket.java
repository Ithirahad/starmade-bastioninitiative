package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.gui.territoryInfo.TerritoryControlOverview;
import org.ithirahad.bastioninitiative.persistence.SystemControlInfo;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static org.ithirahad.bastioninitiative.BastionInitiative.sovContainer;
import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.territoryInfoControlManager;

public class TerritoryInfoPacket extends Packet {
    private ArrayList<TerritoryControlOverview> content;
    private int size;

    public TerritoryInfoPacket(){

    }

    public TerritoryInfoPacket(int factionId) {
        content = new ArrayList<>();
         for(SystemControlInfo info : sovContainer.getAllFactionLocations(factionId)){
             content.add(new TerritoryControlOverview(info));
         }
         size = content.size();
    }
    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        size = b.readInt();
        content = new ArrayList<>();
        for(int i=0; i<size; i++) content.add(b.readObject(TerritoryControlOverview.class));
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(size);
        for(TerritoryControlOverview tc : content){
            b.writeObject(tc);
        }
    }

    @Override
    public void processPacketOnClient() {
        territoryInfoControlManager.updateInfo(content);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {

    }

    public Collection<TerritoryControlOverview> getInfo() {
        return content;
    }
}

package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.vfx.particle.BastionFieldEffects;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

public class HexBurstFXRemoteExecutePacket extends Packet {
    private boolean isExplosion;
    private int sectorId;
    private Vector3f position;
    private float scale;
    private Vector4f sliversColour;

    public HexBurstFXRemoteExecutePacket(int sectorId, Vector3f position, float scale, Vector4f sliversColour, boolean isExplosion) {
        this.sectorId = sectorId;
        this.position = position;
        this.scale = scale;
        this.sliversColour = sliversColour;
        this.isExplosion = isExplosion;
    }

    public HexBurstFXRemoteExecutePacket() {}

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        sectorId = b.readInt();
        position = b.readVector3f();
        scale = b.readFloat();
        sliversColour = b.readVector4f();
        isExplosion = b.readBoolean();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(sectorId);
        b.writeVector3f(position);
        b.writeFloat(scale);
        b.writeVector4f(sliversColour);
        b.writeBoolean(isExplosion);
    }

    @Override
    public void processPacketOnClient() {
        BastionFieldEffects.FireHexBallEffectClient(sectorId,position,scale,sliversColour,isExplosion);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //u wot m8
    }
}

package org.ithirahad.bastioninitiative.listeners;

import api.mod.ModStarter;
import me.iron.WarpSpace.Mod.WarpManager;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLInterdictorAbstract;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.thrust.ThrusterElementManager;

import javax.vecmath.Vector3f;

import static org.ithirahad.bastioninitiative.BastionInitiative.warpSpaceIsPresentAndEnabled;

public class BIInWarpListener implements api.listener.fastevents.ThrusterElementManagerListener{
    Vector3i tmpPos = new Vector3i();
    @Override
    public void instantiate(ThrusterElementManager thrusterElementManager) {

    }

    @Override
    public float getSingleThrust(ThrusterElementManager tem, float v) {
        //"tem"? What is this, an Undertale reference in 2022? kinda cringe tbh.
        // Well, here's a better question,
        // do you wanna have a bad time?
        if(ModStarter.justStartedServer &&
                warpSpaceIsPresentAndEnabled &&
                WarpManager.isInWarp(tem.getSegmentController().getSector(tmpPos))) {
            //...'cause if we don't make sure that we're actually in warp on server first, I don't think you're gonna like what happens next.
            VirtualFTLInterdictorAbstract.doWarpSpaceInterdictorCheck(tem.getSegmentController());
        }
        return v;
    }

    @Override
    public float getSharedThrust(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public float getThrustMassRatio(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public float getMaxSpeed(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public float getMaxSpeedAbsolute(ThrusterElementManager thrusterElementManager, float v) {
        return v;
    }

    @Override
    public Vector3f getOrientationPower(ThrusterElementManager thrusterElementManager, Vector3f v) {
        return v;
    }

    @Override
    public void handle(ThrusterElementManager thrusterElementManager) {

    }

    @Override
    public double getPowerConsumptionResting(ThrusterElementManager thrusterElementManager, double v) {
        return v;
    }

    @Override
    public double getPowerConsumptionCharging(ThrusterElementManager thrusterElementManager, double v) {
        return v;
    }
}

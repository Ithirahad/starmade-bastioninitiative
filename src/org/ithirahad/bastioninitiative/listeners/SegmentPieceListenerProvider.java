package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import api.listener.events.block.SegmentPieceAddByMetadataEvent;
import api.listener.events.block.SegmentPieceAddEvent;
import api.listener.events.block.SegmentPieceRemoveEvent;
import org.schema.game.common.controller.ManagedUsableSegmentController;

import static org.ithirahad.bastioninitiative.BIElementInfoManager.aegisSubsystemControllers;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

public class SegmentPieceListenerProvider {
    public static Listener<?> getBlockListener(Class<?> event) {
        if (event == SegmentPieceAddEvent.class) return new Listener<SegmentPieceAddEvent>() {
            @Override
            public void onEvent(SegmentPieceAddEvent e) {
                if(e.getSegmentController() instanceof ManagedUsableSegmentController<?>) {
                    short id = e.getNewType();
                    if (aegisSubsystemControllers.contains(id) || id == elementEntries.get("Aegis Core").id){
                        /*
                        int count = e.getSegmentController().getElementClassCountMap().get(id);
                        if(count > 0) {
                            e.setCanceled(true);
                            e.getSegment().removeElement(e.getX(), e.getY(), e.getZ(), true);
                            if(e.isServer()) {
                                //idk
                            } else GameClient.getClientPlayerState().sendClientMessage(Lng.str("You cannot place more than one of this block on an entity."), MESSAGE_TYPE_WARNING);
                            return;
                        } else { ...
                         */
                        //Reflective copy of onAddedElementSynched because it refuses to let me set new specialblocks!!!
                        /*
                        ManagerContainer<?> managerContainer = ((ManagedUsableSegmentController<?>) e.getSegmentController()).getManagerContainer();
                        MiscUtils.setPrivateField("flagBlockAdded",managerContainer,true);
                        MiscUtils.setPrivateField("flagAnyBlockAdded",managerContainer, now());
                        ModManagerContainerModule module = managerContainer.getModMCModule(e.getNewType());
                        long absIndex = e.getAbsIndex();
                        if(module != null){
                            int var9 = ByteUtil.modUSeg(ElementCollection.getPosX(absIndex));
                            int var8 = ByteUtil.modUSeg(ElementCollection.getPosY(absIndex));
                            int var6 = ByteUtil.modUSeg(ElementCollection.getPosZ(absIndex));
                            byte var10 = e.getSegment().getSegmentData().getOrientation((byte)var9, (byte)var8, (byte)var6);
                            module.handlePlace(absIndex, var10);
                        }
                         */
                        //...}
                    } //TODO: removing while placing is a bad idea; need proper function for this

                }
            }
        };
        else if (event == SegmentPieceAddByMetadataEvent.class) return new Listener<SegmentPieceAddByMetadataEvent>() {
            @Override
            public void onEvent(SegmentPieceAddByMetadataEvent e) {

            }
        };
        if (event == SegmentPieceRemoveEvent.class) return new Listener<SegmentPieceRemoveEvent>() {
            @Override
            public void onEvent(SegmentPieceRemoveEvent segmentPieceRemoveEvent) {
                //TODO: ditto
            }
        };
        else return null;
    }
}

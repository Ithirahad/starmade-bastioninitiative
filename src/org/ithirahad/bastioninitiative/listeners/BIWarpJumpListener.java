package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import com.bulletphysics.dynamics.RigidBody;
import me.iron.WarpSpace.Mod.WarpJumpEvent;
import me.iron.WarpSpace.Mod.WarpManager;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLInterdictorAbstract;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLTrap;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

import javax.vecmath.Vector3f;
import java.util.HashMap;
import java.util.Random;

import static me.iron.WarpSpace.Mod.WarpJumpEvent.WarpJumpType.ENTRY;
import static org.ithirahad.bastioninitiative.util.BIUtils.nextSignedFloat;
import static org.schema.common.FastMath.round;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BIWarpJumpListener extends Listener<WarpJumpEvent> {
    private static final HashMap<String, VirtualFTLInterdictorAbstract> dropCache = new HashMap<>();

    public static void enqueueDropLocation(String uniqueIdentifier, VirtualFTLTrap targetLoc) {
        dropCache.put(uniqueIdentifier,targetLoc);
    }

    public static boolean shipQueued(String uniqueIdentifier) {
        return dropCache.containsKey(uniqueIdentifier);
    }

    @Override
    public void onEvent(WarpJumpEvent e) {
        if(e.isServer()){
            if(e.getType() != ENTRY) {
                String uid = e.getShip().getUniqueIdentifier();
                if (dropCache.containsKey(uid)) {
                    VirtualFTLInterdictorAbstract vfd = dropCache.get(uid);
                    boolean isTrap = vfd instanceof VirtualFTLTrap;
                    if(isTrap) e.getEnd().set(vfd.getParent().getSectorLocation());
                    else{
                        Vector3i nearestJP = WarpManager.getRealSpacePos(e.getShip().getSector(new Vector3i()));
                        Vector3i structure = vfd.getParent().getSectorLocation();
                        if(structure.equals(nearestJP)){
                            Random rng = new Random();
                            Vector3f os = new Vector3f(nextSignedFloat(rng),nextSignedFloat(rng),nextSignedFloat(rng)); //offset
                            os.normalize();
                            os.scale(vfd.getRange());
                            os.add(vfd.getParent().getSectorLocation().toVector3f());
                            e.getEnd().set(round(os.x), round(os.y), round(os.z));
                            //if the nearest drop point is literally right on the repulsion station, eject the dropping ship onto the bubble surface in a random direction
                        }
                        else{
                            Vector3f v = structure.toVector3f();
                            v.sub(nearestJP.toVector3f()); //deflection vector
                            v.normalize();
                            v.scale(vfd.getRange()); //to surface of bubble
                            v.add(nearestJP.toVector3f());
                            e.getEnd().set(round(v.x), round(v.y), round(v.z));
                            //Put the ship on the nearest point on the bubble surface, to the drop point. (exploitable to use like an FTL trap, but unless WarpSpace updates, IDC)
                        }
                    }
                    RigidBody body = ((SegmentController)e.getShip()).getPhysicsObject();
                    float originalLin = body.getLinearDamping();
                    float originalAng = body.getAngularDamping();
                    body.setDamping(1.0f, 1.0f);
                    body.applyDamping(1);
                    body.setDamping(originalLin, originalAng);
                    body.activate(true); //stop the boat.

                    String message;
                    if(isTrap) message = "Your vessel has been pulled to the FTL Interceptor at " + vfd.getParent().getSectorLocation() + ".";
                    else message = "Your warp has been disrupted by an FTL Repeller Field at " + vfd.getParent().getSectorLocation() + ".\r\n Ship is being redirected to the edge of the field!";
                    for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>) e.getShip()).getAttachedPlayers()) {
                        playerAboard.sendServerMessage(Lng.astr(message), MESSAGE_TYPE_INFO);
                    }
                    dropCache.remove(uid);
                }
            }
        }
    }
}

package org.ithirahad.bastioninitiative.persistence;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.network.objects.remote.FleetCommand;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import static org.ithirahad.bastioninitiative.BISovereigntyConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.BastionInitiative.sovContainer;
import static org.schema.common.FastMath.floor;
import static org.schema.game.common.data.fleet.FleetCommandTypes.*;

/**
    An info sheet containing data about a given faction's control of a star system.
 */
public class FactionSystemControlSheet implements Serializable {
    //should PlayerFactionControlSheet, NPCFactionControlSheet, and NeutralControlSheet be separate?
    //it will create serialization issues but would save on special casing in methods.
    public final int factionID;
    private boolean homebase = false;
    private final Vector3i location;
    private transient SystemControlInfo parent; //star system coordinates
    private final HashSet<String> factionStructures; //UIDs of stations and planets owned by the faction
    private final HashSet<String> factionPlanets; //UIDs of planet cores
    private final LongOpenHashSet factionFleets;
    private final Object2IntOpenHashMap<Vector3i> recievesAdjacencyBonusFrom = new Object2IntOpenHashMap<Vector3i>(); //map of source systems to adjacency bonuses from those systems
    private final ArrayList<Vector3i> givingAdjacencyBonusTo = new ArrayList<>(); //list of systems recieving bonus from this system (actual number is always the same for all)
    int lastInfluenceWithoutAdjacency = 0;
    protected float glory = 0;
    private long lastUpdated;
    private String nexusUID;

    protected FactionSystemControlSheet(int factionID, Vector3i starSystem) {
        this.factionStructures = new HashSet<>();
        this.factionPlanets = new HashSet<>();
        this.factionFleets = new LongOpenHashSet();
        this.factionID = factionID;
        this.location = new Vector3i(starSystem);
        this.parent = sovContainer.getOrCreateInfo(starSystem);
        nexusUID = null;
        lastUpdated = 0;
        for(Vector3i dir : Element.DIRECTIONSi){
            Vector3i loc = new Vector3i(location);
            loc.add(dir);
            givingAdjacencyBonusTo.add(loc);
            recievesAdjacencyBonusFrom.add(loc,0);
        }
    }

    public void afterDeserialize(){
        //for future use
    }

    protected void updateInfluence(long timeOfUpdate) { //should really only be called by the parent SystemControlInfo.
        if(lastUpdated != timeOfUpdate) {
            //we don't necessarily know if, due to some future feature, an update triggered another update, so this can be used to determine if another distinct call to update is needed or not
            int influence = 0;
            influence += getHomebaseInfluence();
            influence += getStructuresInfluence();
            influence += getFleetsInfluence();
            influence += getPlanetsInfluence();
            influence += getNexusInfluence();
            influence += getInfluenceFromGlory();

            influence *= getFPInfluenceModifier();

            lastInfluenceWithoutAdjacency = influence;
            int givingBonus = getBonusToAdjacent();
            if(givingBonus > 0){
                for(Vector3i adjacentSystem : givingAdjacencyBonusTo){
                    if(!sovContainer.hasFactionAt(adjacentSystem, factionID)){
                        SystemControlInfo si = sovContainer.getOrCreateInfo(adjacentSystem);
                        si.putFaction(factionID);
                    }
                    FactionSystemControlSheet adjacent = sovContainer.getFactionSheet(factionID,adjacentSystem);
                    adjacent.setRecievedAdjacencyBonus(location,givingBonus);
                }
            }
            lastUpdated = timeOfUpdate;
        }
    }

    private float getFPInfluenceModifier() {
        float modifier;
        float factionPoints = parent.getFactionManager().getFaction(factionID).factionPoints;
        if(factionPoints < 0) modifier = NEGATIVE_FACTIONPOINTS_INFLUENCE_MODIFIER;
        else if(factionPoints < LOW_FACTIONPOINTS_THRESHOLD) modifier = LOW_FACTIONPOINTS_INFLUENCE_MODIFIER;
        else modifier = 1;
        return modifier;
    }

    private int getInfluenceFromGlory() {
        return (int) (glory * INFLUENCE_PER_CLAIMED_GLORY);
    }

    private int getNexusInfluence() {
        int influence = 0;
        if (hasActiveAndLinkedNexus()){
            influence += INFLUENCE_BASE_FROM_STELLAR_NEXUS;
            VirtualAegisSystem aegisSystem = bsiContainer.getAegisSystemByID(nexusUID);
            if (aegisSystem.getFactionID() == factionID && aegisSystem.hasSubsystem("Aegis Stellar Nexus")) {
                VirtualStellarNexus nexus = (VirtualStellarNexus) aegisSystem.getSubsystem("Aegis Stellar Nexus");
                if(nexus.isOperating(false)) {
                    float influenceBonus = floor(nexus.getNetworkTotalStructurePoints() * INFLUENCE_FROM_STELLAR_NEXUS_PER_LINKED_STRUCT_POINT);
                    influence += (int) influenceBonus;
                }
            }
        }
        return influence;
    }

    private int getPlanetsInfluence() {
        return factionPlanets.size() * INFLUENCE_PER_OWN_PLANET_BASE;
    }

    private int getFleetsInfluence() {
        boolean anyActiveFleet = false;
        int result = 0;
        if(!factionFleets.isEmpty()){
            for(Long uid : factionFleets){
                Fleet fleet = GameServerState.instance.getFleetManager().getByFleetDbId(uid);

                if(
                    fleet.getMass() > MASS_THRESHOLD_FOR_FLEET_INFLUENCE &&
                    fleet.getMembers().size() > MINIMUM_MEMBERS_FOR_FLEET_INFLUENCE
                )
                {
                    FleetCommand cmd = (FleetCommand) MiscUtils.readPrivateField("currentCommand",fleet);

                    if( cmd != null &&
                        cmd.getCommand() != IDLE.ordinal() &&
                        cmd.getCommand() != FLEET_IDLE_FORMATION.ordinal() &&
                        cmd.getCommand() != CALL_TO_CARRIER.ordinal()
                    )
                    {
                        anyActiveFleet = true;
                        break; //temp
                        //TODO: influence per fleet mass with diminishing returns?
                    }
                }
            }
        }
        if(anyActiveFleet) result += INFLUENCE_FROM_ANY_ACTIVE_FLEET_STARSYSTEM;
        return result;
    }

    private int getStructuresInfluence() {
        if (!factionStructures.isEmpty()) {
            if (parent.isStarSystem())
                return INFLUENCE_FROM_ANY_CLAIMED_STRUCTURE_STARSYSTEM;
            else return INFLUENCE_POINTS_FROM_ANY_CLAIMED_STRUCTURE_VOID;
        }
        else return 0;
    }

    private int getHomebaseInfluence() {
        if (parent.getFirstHomebaseOwner() == factionID)
            return INFLUENCE_FROM_FIRST_HOMEBASE;
        else return 0;
    }

    public boolean hasActiveAndLinkedNexus() {
        if(nexusUID == null) return false;
        else{
            VirtualAegisSystem sys = bsiContainer.getAegisSystemByID(nexusUID);
            if(sys == null) return false;
            else if(sys.getFactionID() != factionID){
                setNexusUID(null);
                return false;
                //TODO system updating its faction ID should trigger reregistration from its side.
                // onFactionChange() should be a thing for virtual aegis systems in general.
                // this sort of weird side effect is spaghetti af and should not be necessary.
            }
            else{
                VirtualStellarNexus nexus = (VirtualStellarNexus) sys.getSubsystem("Aegis Stellar Nexus");
                boolean operating = nexus.isOperating(false);
                boolean hasNetwork = nexus.hasAnyLinked();
                if(operating && hasNetwork) return true;
                else return false;
            }
        }
    }

    public void setNexusUID(String uid){
        nexusUID = uid;
    }

    public int getInfluence(boolean includeAdjacency){
        if(includeAdjacency){
            int adj = 0;
            for(int bonus : recievesAdjacencyBonusFrom.values()) adj += bonus;
            return lastInfluenceWithoutAdjacency + adj;
        }
        else return lastInfluenceWithoutAdjacency;
    }

    public int getBonusToAdjacent(){
        if(getInfluence(true) > MINIMUM_LOCAL_INFLUENCE_POINTS_FOR_ADJACENCY_BONUS){
            return (int)FastMath.floor(lastInfluenceWithoutAdjacency * ADJACENCY_BONUS_BASE_MULTIPLIER);
        }
        else return 0;
    }

    public String getFactionName(){
        if(factionID == -1) return "Neutral";
        else{
            return parent.getFactionManager().getFactionName(factionID);
        }
    }

    public Vector3i getSystemLocation(){
        return new Vector3i(location);
    }

    public void reset() {
        Iterator<Vector3i> adjacencies = getAdjacencyProviders();
        Vector3i v;
        FactionSystemControlSheet sheet;
        while(adjacencies.hasNext()){
            v = adjacencies.next();
            sheet = sovContainer.getFactionSheet(factionID,v);
            sheet.setRecievedAdjacencyBonus(location,0);
            if(sheet.getInfluence(true) <= 0) sovContainer.removeFactionFromSystem(v,factionID);
            //clean up after ourselves
        }
    }

    public void setRecievedAdjacencyBonus(Vector3i from, int val) {
        recievesAdjacencyBonusFrom.put(from,val);
    }

    public Iterator<Vector3i> getAdjacencyProviders() {
        return recievesAdjacencyBonusFrom.keySet().iterator();
    }

    public Iterator<Vector3i> getAdjacencyBenefactors(){
        return givingAdjacencyBonusTo.iterator();
    }

    public void addFactionStructure(String uid) {
        factionStructures.add(uid);
        Sendable obj = GameServerState.instance.getLocalAndRemoteObjectContainer().getUidObjectMap().get(uid);
        if(obj instanceof Planet){
            factionPlanets.add(((Planet)obj).getPlanetCoreUID());
        }
    }

    public void removeFactionStructure(String uid) {
        factionStructures.remove(uid);
        Sendable obj = GameServerState.instance.getLocalAndRemoteObjectContainer().getUidObjectMap().get(uid);
        if(obj instanceof Planet){
            factionPlanets.remove(((Planet)obj).getPlanetCoreUID());
        }
    }

    public boolean hasPlanet(String coreUID) {
        return factionPlanets.contains(coreUID);
    }

    public void setParent(SystemControlInfo s) {
        parent = s;
    }

    public String getNexusUID() {
        return nexusUID;
    }

    /*
    TODO:
     <<Create config for permanent/temporary/external-contributor factors, and adjacency-sharing-management system>>
     <<maybe ClaimPointsContributor interface with getContribution(Vector3i systemLoc) method?>>
     **GENERAL**
     The more control a faction has, the better their mining bonus, and at certain control levels new options open up.
     e.g. a faction can levy taxes on shops above 70% control,
     and - provisionally, pending Phase 3 station systems - have better intel on stuff in their territory above say 50% control.
     Whichever faction has a plurality of control gets the technical "claim" on the system though.
     ...
     **NPC INTERACTION**
     Force 100% control for NPCs in NPC territory for now.
     Otherwise, systems start off with some % "Neutral" control (immediately filled by claiming faction)
     and the remaining % "Outlaw" control (slowly fills by claiming faction if uncontested; fill rate boosted by eliminating/neutralizing pirate stations)
     The ratio depends on the amount of NPC stations - we will assume that 1/3 belong to Pirates and work from there.
     ...
     **CONTIGUITY**
     In order for contiguous territory to matter,
     owning a neighboring territory should boost your claim by 2x iff you have any source of claim at all.
     Being a homebase should boost your claim by like 3x.
     (On that note, maybe homebase invulnerability should drop if, despite the giant claim boost, you manage to lose the plurality of control.
     At least, it should be an option.)
     ...
    */
}

package org.ithirahad.bastioninitiative.persistence;

import api.common.GameServer;
import api.listener.events.entity.ShipJumpEngageEvent;
import api.utils.sound.AudioUtils;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

import javax.vecmath.Vector3f;

import static org.ithirahad.bastioninitiative.BIConfiguration.AEGIS_FTL_WALL_ALLOWED;
import static org.ithirahad.bastioninitiative.util.BIUtils.dSquared;
import static org.ithirahad.bastioninitiative.util.BIUtils.nearestApproachOnLine;
import static org.schema.common.FastMath.round;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class VirtualFTLWall extends VirtualFTLInterdictorAbstract {
    @Override
    public String getBlockEntryName() {
        return "Aegis FTL Wall";
    }

    @Override
    public boolean isOperating(boolean respectDisruptionStatus) {
        return super.isOperating(respectDisruptionStatus) && AEGIS_FTL_WALL_ALLOWED;
    }

    @Override
    public void handleEntityJumping(ShipJumpEngageEvent info) {
        Vector3i jumpOrigin = info.getOriginalSectorPos();
        Vector3i originalDest = new Vector3i(info.getNewSector());
        Vector3i structureLocation = getParent().getSectorLocation();
        Vector3i newDest = new Vector3i();

        boolean doRedirect = true;

        long startDistanceFromStructureSquared = Vector3i.getDisatanceSquaredD(jumpOrigin,structureLocation);

        String message = "";
        String factionName;
        if(getFactionID() == 0) factionName = "a neutral structure"; //default text here would be "None", leaving the very silly "None's FTL Repeller". Let's not have that.
        else factionName = GameServer.getServerState().getFactionManager().getFactionName(getFactionID());

        if(startDistanceFromStructureSquared > rangeSquared()){
            //This is the case where someone's trying to jump in from outside. We have to catch the jump where it intersects with the repulsion bubble.
            message = "WARNING: Jump path crosses " + factionName + "'s FTL Repeller Field in system!\r\n Original destination unreachable; jumping to field perimeter.";
            Vector3i jumpVector = new Vector3i(originalDest);
            jumpVector.sub(jumpOrigin); //direction of jump

            //In this case, there is a right triangle between the theoretical closest approach point (ignoring jump range), intersection of jump path and range bubble, and the repelling structure's location.
            //We can use this to solve for the amount of jump length that needs to be truncated between closest approach and the edge of the bubble,
            //Thereby giving us the first intersection point (where the ship will ultimately end up)
            Vector3f closestApproach = nearestApproachOnLine(jumpOrigin,originalDest,structureLocation,false);
            //The only necessary constraint is simply that the true, range-limited line segment of the jump crosses the bubble at all - already checked by the time this method is called.
            float c2 = rangeSquared();
            //the hypotenuse's length is the range of the field itself - naturally, this spans from the station to the point where the oncoming ship must stop.
            float a2 = dSquared(closestApproach,structureLocation.toVector3f());
            //the known leg of the triangle is the distance between the theoretical point of nearest approach, and the location of the structure.
            float b = FastMath.sqrt(c2 - a2);
            //this is the mystery leg. Shortening the path from jump starting point to closest approach by this quantity gives the final jump distance.

            Vector3f idealJump = jumpOrigin.toVector3f();
            idealJump.sub(closestApproach);//"ideal" jump between origin and closest approach.
            float idealL = idealJump.length();
            float realL = jumpVector.length();
            b += (realL - idealL); //have to make sure this covers overshoots just as correctly as undershoots... leaving us a "real" B value.
            //that is to say - the actual length that needs to be lopped off the jump.
            float remainderRatio = (realL - b)/realL; //ratio of lengths between the initially-intended jump and the field-affected one
            Vector3f newJumpVector = MiscUtils.lerp(jumpOrigin.toVector3f(),originalDest.toVector3f(),remainderRatio); //final jump vector.
            newDest.set(round(newJumpVector.x),round(newJumpVector.y),round(newJumpVector.z));
        } else if(Vector3i.getDisatanceSquaredD(structureLocation,originalDest) < rangeSquared()) {
            //...Else, someone is starting from within the bubble and trying to jump to somewhere else inside. True to its name, the field overrides ship nav and expels the ship.
            message = "WARNING: Jump path is within " + factionName + "'s FTL Repeller Field in system!\r\n Ship is being deflected to the edge of the field.";
            Vector3i referenceTarget = new Vector3i();
            if(originalDest.equals(structureLocation)) referenceTarget.set(jumpOrigin); //if trying to jump directly at the structure, you get bounced out the exact other way.
            else referenceTarget.set(originalDest);
            newDest.set(jumpOrigin);
            newDest.sub(structureLocation); //opposite dir
            Vector3f vf = newDest.toVector3f(); //deflected vector
            vf.normalize();
            vf.scale(range);
            vf.add(structureLocation.toVector3f());
            newDest.set(round(vf.x), round(vf.y), round(vf.z));
        } else doRedirect = false; //Else, they're leaving anyway. No need to bother them.

        if(doRedirect) {
            info.getNewSector().set(newDest); //redirect the jump
            for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>) info.getController()).getAttachedPlayers()) {
                //TODO: time-delayed action, sound effect, particle effects, etc
                playerAboard.sendServerMessage(Lng.astr(message), MESSAGE_TYPE_INFO);
            }
            AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, ((ManagedUsableSegmentController<?>) info.getController()).getAttachedPlayers());
        }
    }

    @Override
    public void handleEntityWarpSpace(ManagedUsableSegmentController<?> entity) {
        // TODO: maybe use kinetics to repel ships??? WS may be abandoned so no need to think too hard about this I guess, but I don't want to drop support (yet)
    }
}

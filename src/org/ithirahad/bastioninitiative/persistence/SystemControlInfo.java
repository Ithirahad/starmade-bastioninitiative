package org.ithirahad.bastioninitiative.persistence;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntSet;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.ithirahad.bastioninitiative.util.TemporalShortcuts;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.game.common.data.element.Element;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.space.PlanetCore;
import org.schema.game.server.data.GameServerState;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Objects;

import static org.ithirahad.bastioninitiative.BISovereigntyConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.sovContainer;
import static org.ithirahad.bastioninitiative.util.BIUtils.isStar;
import static org.ithirahad.bastioninitiative.util.BIUtils.setFactionOwnershipWithoutClaimStation;
import static org.schema.common.FastMath.floor;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

public class SystemControlInfo implements Serializable {
    private final Vector3i location; //Coordinates of star system
    private final Int2ObjectOpenHashMap<FactionSystemControlSheet> factionSheets = new Int2ObjectOpenHashMap<>();
    private LinkedList<Integer> homebaseOwners; //Homebase owner factions in system, in order of establishment.
    private ObjectOpenHashSet<String> homebaseUIDs; //record of known homebases in system
    private int controllingFaction = 0;
    private int unclaimedGlory;
    private int neutralInfluenceFromStructure; //depleted by players claiming pirate structures
    private int neutralInfluenceFromPVE; //depleted by killing pirates
    private long lastNeutralInfluenceUpdate;
    final Object2IntOpenHashMap<String> structuresToFactions = new Object2IntOpenHashMap<>(); //sectors to UIDs to factions
    public SystemControlInfo(Vector3i system) {
        this.location = system;
        this.homebaseOwners = new LinkedList<>();
        this.homebaseUIDs = new ObjectOpenHashSet<>();
        this.unclaimedGlory = SYSTEM_GLORY_POOL;
        this.neutralInfluenceFromStructure = NEUTRAL_PVE_STRUCTURE_INFLUENCE_POOL_STARSYSTEM;
        this.neutralInfluenceFromPVE = NEUTRAL_PVE_BATTLE_INFLUENCE_POOL_STARSYSTEM;
    }

    /**
     * @param factionId Faction ID whose perspective the overview should be written from. 0 for no faction.
     * @param sys Coordinates of the star/void system block in question
     * @return
     */
    public static String getOverviewString(int factionId, Vector3i sys) {
        String message;
        SystemControlInfo systemInfo = sovContainer.getInfo(sys);
        if (systemInfo == null) message = "This system is currently neutral.\r\nNo faction has significant influence.";
        else {
            FactionManager facman = GameServerState.instance.getFactionManager();
            int owner = systemInfo.getControllingFactionID();
            String relation = "";
            if (owner == 0) {
                message = "This system is currently neutral.";
                if(isStar(sys)) message += "\r\nIt is dominated by the pirates, outlaws, and private spacefarers that currently call it home.";
                else message += "\r\nApart from the occasional pirate nest or Guild trading post, nobody can rightly claim ownership to this barren expanse.";
                message += "\r\nCurrent neutral influence level: " + systemInfo.getNeutralInfluence();
            } else {
                String ownerName = systemInfo.getControllingFactionName();
                if (factionId == owner) relation = "your own faction, ";
                else {
                    FactionRelation.RType facmanRelation = facman.getRelation(factionId, owner);//otherwise leave it blank
                    if (Objects.requireNonNull(facmanRelation) == FactionRelation.RType.ENEMY) {
                        relation = "your enemies, ";
                    } else if (facmanRelation == FactionRelation.RType.FRIEND) {
                        relation = "your allies, ";
                    } //otherwise leave it blank
                }
                message = "This system is currently occupied by " + relation + ownerName + ".\r\n";
                message += ownerName + "'s current influence level: " + systemInfo.getInfluence(owner);
            }
            if (factionId != 0 && factionId != owner && systemInfo.factionIsPresent(factionId)) {
                if (systemInfo.factionIsPresent(factionId)) {
                    message += "\r\n\r\nYour faction, " + facman.getFactionName(factionId) + ", maintains a presence in this system.\r\n";
                    message += facman.getFactionName(factionId) + "'s current influence level: " + systemInfo.getInfluence(factionId);
                }
            }
        }
        return message;
    }

    public Vector3i getLocation(){
        return new Vector3i(location);
    }

    protected void putFaction(int factionID) {
        FactionSystemControlSheet sheet = new FactionSystemControlSheet(factionID,location);
        factionSheets.put(sheet.factionID,sheet);
    }

    public FactionSystemControlSheet getFactionSheet(int factionID) {
        return factionSheets.get(factionID);
    }

    public boolean isEmpty() {
        return factionSheets.isEmpty()
                && neutralInfluenceFromStructure == NEUTRAL_PVE_STRUCTURE_INFLUENCE_POOL_STARSYSTEM
                && neutralInfluenceFromPVE == NEUTRAL_PVE_BATTLE_INFLUENCE_POOL_STARSYSTEM;
        //if no present factions, and neutral influence is up to full, this doesn't need to exist
    }

    public void removeFaction(int factionID) {
        factionSheets.remove(factionID);
    }

    public void clear() {
        for(FactionSystemControlSheet sheet : factionSheets.values()) sheet.reset(); //zero out adjacency bonuses, etc
    }

    public void update(){
        long startOfUpdate = System.currentTimeMillis();
        IntIterator iterator = factionSheets.keySet().iterator();
        while(iterator.hasNext()) {
            FactionSystemControlSheet sheet = factionSheets.get(iterator.next());
            sheet.updateInfluence(startOfUpdate);
        }
        for(SystemControlInfo info : getSelfAndAdjacent(true)) {
            info.updateOwnership();
        }
    } //TODO: un-expose this and implement/expose requestUpdate() and waitForUpdate(someCallable) to avoid pointless duplication

    private SystemControlInfo[] getSelfAndAdjacent(boolean createIfMissing) {
        SystemControlInfo[] result = new SystemControlInfo[7];
        Vector3i starSystem = new Vector3i();
        result[0] = this;
        for(int i = 1; i<=6; i++){
            starSystem.set(location);
            starSystem.add(Element.DIRECTIONSi[i-1]);
            if(createIfMissing) result[i] = sovContainer.getOrCreateInfo(starSystem);
            else result[i] = sovContainer.getInfo(starSystem);
        }
        return result;
    }

    private void updateOwnership(){
        IntIterator iterator = factionSheets.keySet().iterator();
        FactionSystemControlSheet highestInfluencer = null;
        int highestInfluence = 0;
        while(iterator.hasNext()){
            FactionSystemControlSheet sheet = factionSheets.get(iterator.next());
            int totalInfluence = sheet.getInfluence(true);
            if(totalInfluence <= 0) iterator.remove(); //clean up factions with no claim
            else {
                if (totalInfluence > highestInfluence) {
                    highestInfluencer = sheet;
                    highestInfluence = totalInfluence;
                }
            }
        }
        if(highestInfluence > getNeutralInfluence()){
            if(highestInfluencer.factionID != controllingFaction)
                setControllingFaction(highestInfluencer.factionID);
        }
        else if(controllingFaction != 0)
            setControllingFaction(0); //nobody has enough influence; system is neutral
    }

    private void setControllingFaction(int factionID) {
        try {
            String reason;
            setFactionOwnershipWithoutClaimStation(factionID,location,getFactionManager().getFactionName(factionID),false);
            if(controllingFaction > 0){
                String loserInfo;
                if(factionID > 0) loserInfo = "Your influence within the system has been overshadowed by " + GameServerState.instance.getFactionManager().getFactionName(factionID) + ".";
                else loserInfo = "Due to your lack of influence, it has fallen back into lawlessness.";

                String message = "Your faction has lost control of the " + BIUtils.getSystemName(location) + " system at " + location + "!\r\n" + loserInfo;
                BIUtils.sendAllMembersMessage(message,controllingFaction,MESSAGE_TYPE_WARNING);
            }
            if(factionID > 0){
                String winnerInfo;
                if(controllingFaction > 0) winnerInfo = "Your presence and activity within the system has overshadowed the influence of " + GameServerState.instance.getFactionManager().getFactionName(controllingFaction) + ".";
                else winnerInfo = "You have brought a measure of order to this once-lawless region of space.";

                String message = "The " + BIUtils.getSystemName(location) + " system (" + location + ") is now within your territory!\r\n" + winnerInfo;
                BIUtils.sendAllMembersMessage(message,factionID,MESSAGE_TYPE_INFO);
            }
            controllingFaction = factionID;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getNeutralInfluence() {
        long now = System.currentTimeMillis();
        if(neutralInfluenceFromPVE < NEUTRAL_PVE_BATTLE_INFLUENCE_POOL_STARSYSTEM) {
            long timeSinceUpdate = now - lastNeutralInfluenceUpdate;
            double days = (double) timeSinceUpdate / (double) TemporalShortcuts.MS_TO_DAYS;
            int diff = NEUTRAL_PVE_BATTLE_INFLUENCE_POOL_STARSYSTEM - neutralInfluenceFromPVE;
            neutralInfluenceFromPVE += Math.min(diff,(int)Math.floor(NEUTRAL_PVE_BATTLE_INFLUENCE_REGEN_PER_DAY*days));
        }
        lastNeutralInfluenceUpdate = now;
        int baseInfluence = isStarSystem()? NEUTRAL_INFLUENCE_BASE_STARSYSTEM : NEUTRAL_INFLUENCE_BASE_VOID;
        return neutralInfluenceFromPVE + neutralInfluenceFromStructure + baseInfluence;
    }

    public int getFirstHomebaseOwner(){
        if(!homebaseOwners.isEmpty()) return homebaseOwners.getFirst();
        else return 0;
    }

    public void addHomebaseOwner(int factionID){
        //TODO validate if it's actually a faction
        homebaseOwners.add(factionID);
    }

    public void removeHomebaseOwner(int factionID){
        homebaseOwners.remove((Integer)factionID);
    }

    public boolean factionIsPresent(int factionID) {
        return factionSheets.containsKey(factionID);
    }

    public boolean isStarSystem() {
        return isStar(location);
    }

    public int getControllingFactionID() {
        return controllingFaction;
    }

    public String getControllingFactionName(){
        return getFactionManager().getFactionName(controllingFaction);
    }

    static FactionManager getFactionManager() {
        return GameServerState.instance.getFactionManager();
    }

    public boolean hasUnclaimedGlory(){
        return unclaimedGlory > 0;
    }

    public void giveUnclaimedGloryPvE(int recievingFaction, int amount){
        if(hasUnclaimedGlory() && factionIsPresent(recievingFaction)){
            FactionSystemControlSheet factionInfo = getFactionSheet(recievingFaction);
            if(amount <= unclaimedGlory){
                factionInfo.glory += amount;
                unclaimedGlory -= amount;
            } else {
                factionInfo.glory += unclaimedGlory;
                unclaimedGlory = 0;
            }
        }
    }

    public void giveGloryInclFromFactions(int recievingFaction, int amount){
        if(factionIsPresent(recievingFaction)){
            FactionSystemControlSheet factionInfo = getFactionSheet(recievingFaction);
            if(amount <= unclaimedGlory){
                factionInfo.glory += amount;
                unclaimedGlory -= amount;
            } else {
                float diff = amount - unclaimedGlory; //difference to make up
                int obtained = 0;
                int factions = factionSheets.size() - 1;
                float lostShare = (int) floor(diff/factions);
                for(FactionSystemControlSheet sheet : factionSheets.values()){
                    if(sheet.factionID != recievingFaction){
                        if(lostShare <= sheet.glory) {
                            sheet.glory -= lostShare;
                            obtained += lostShare;
                        }
                        else{
                            obtained += sheet.glory;
                            sheet.glory = 0f;
                            //take whatever's there lol
                        }
                    }
                } //try to make it up, won't get all the way there but whatever.
                // The "right" way would be to pare the list down to factions with enough
                // which is an unreasonable amount of effort for this weird niche feature


                factionInfo.glory += unclaimedGlory;
                factionInfo.glory += obtained;
                unclaimedGlory = 0;
            }
        }
    }

    public void giveGloryPvP(int losingFaction, int winningFaction, int amount) {
        if (factionIsPresent(winningFaction) && factionIsPresent(losingFaction)) { //actual contest
                FactionSystemControlSheet loser = getFactionSheet(losingFaction); //nerds.
                FactionSystemControlSheet winner = getFactionSheet(winningFaction);
                if(loser.glory >= amount){
                    loser.glory -= amount;
                    winner.glory += amount;
                    //simple case. Loser has something to lose, and they lose it.
                }
                else{
                    float remainder = amount - loser.glory;
                    if(remainder <= unclaimedGlory) {
                        loser.glory = 0;
                        unclaimedGlory -= remainder;
                        winner.glory += amount;
                        //unclaimed amount makes up the difference
                    }
                    else{
                        winner.glory += unclaimedGlory;
                        winner.glory += loser.glory;
                        //winner takes whatever's left
                    }
                }
        } else giveUnclaimedGloryPvE(winningFaction,amount); //skirmish or something outside of occupied space, just steal some glory off them lmao
    }

    public void addOrUpdateTrackedStructure(SegmentController obj){
        Integer currentEntityFaction = obj.getFactionId();
        String uid = obj.getUniqueIdentifier();
        Integer originalRecordedFaction;

        if(structuresToFactions.containsKey(uid)) originalRecordedFaction = structuresToFactions.get(uid);
        else originalRecordedFaction = null;
        if (!currentEntityFaction.equals(originalRecordedFaction)) {
            //Structure was taken over (or handed over)
            if(!obj.isCoreOverheating()) {
//Overheat event does similar processing, and once the object is overheating the faction info is wiped out anyway
                if(originalRecordedFaction != null){ //faction was present in SystemControlInfo's registry
                    int damagerIndex = (Integer) MiscUtils.readPrivateField("lastDamager",obj.getHpController());
                    Damager damager = ((Damager) obj.getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(damagerIndex));
                    if(originalRecordedFaction == -1){ //pirate struct
                        if(damager != null) {
                            handleBlockEntityDestruction(obj, damager.getFactionId(), originalRecordedFaction); //we count this as a kill, as the station may not be overheatable anyway
                            onPirateStructureUnclaimed(uid);
                            structuresToFactions.remove(uid); //cleanup
                            update();
                        }
                    } else if(originalRecordedFaction > 0 && currentEntityFaction > 0){
                        //Station changed hands between players
                        handleEntityCaptured(obj,currentEntityFaction,originalRecordedFaction); //we count this as a kill, as the station may not be overheatable anyway
                    }
                } else { //entity had no faction before, as far as we know
                    if (currentEntityFaction != 0) structuresToFactions.put(uid, currentEntityFaction);
                    if (currentEntityFaction > 0) { //player faction
                        FactionSystemControlSheet sheet = getOrAddFactionSheet(currentEntityFaction);
                        sheet.addFactionStructure(uid);
                        update();
                    }  //else NPC, pretend it doesn't exist :P
                }
            } else {
                //overheat event should've taken care of it
                removeStructure(uid); //overheating structure; get rid of it
            }
        } //otherwise nothing has changed; we already have the correct info for this structure
        if(obj.isHomeBase()) addOrUpdateHomeBase(obj);
        else if(homebaseUIDs.contains(obj.getUniqueIdentifier())) removeHomeBase(obj);
    }

    public void handleEntityCaptured(SegmentController obj, Integer winningFactionID, Integer losingFactionID) {
        structuresToFactions.put(obj.getUniqueIdentifier(),winningFactionID); //cleanup
        if(isWarTargetTo(winningFactionID,losingFactionID)){
            int glory;
            SimpleTransformableSendableObject.EntityType type = obj.getType();
            if(type == SPACE_STATION || type == PLANET_ICO || type == PLANET_SEGMENT) glory = PVP_GLORY_PER_STATION_KILL;
            else glory = PVP_GLORY_PER_KILL;
            giveGloryPvP(losingFactionID,winningFactionID,glory);
        }
        update();
    }

    private void removeStructure(SegmentController entity){
        if(getPresentFactions().contains(entity.getFactionId())){
            removeStructure(entity.getUniqueIdentifier());
            if(entity.isHomeBase())
                removeHomeBase(entity);
        }
    }
    private void removeStructure(String uid) {
        int faction = structuresToFactions.remove(uid);
        FactionSystemControlSheet sheet = getFactionSheet(faction);
        if(sheet != null) sheet.removeFactionStructure(uid);
        update();
    }

    private void removeHomeBase(SegmentController obj) {
        removeHomebaseOwner(obj.getFactionId());
        homebaseUIDs.remove(obj.getUniqueIdentifier());
    }

    private void onPirateStructureUnclaimed(String uid) {
        neutralInfluenceFromStructure -= NEUTRAL_PVE_STRUCTURE_UNCLAIM_LOSS;
        if(neutralInfluenceFromStructure < 0) neutralInfluenceFromStructure = 0;
    }

    public FactionSystemControlSheet getOrAddFactionSheet(int factionID) {
        FactionSystemControlSheet result = getFactionSheet(factionID);
        if(result == null) return sovContainer.addFactionToSystem(location,factionID);
        else return result;
    }

    public IntSet getPresentFactions() {
        return factionSheets.keySet();
    }

    private void addOrUpdateHomeBase(SegmentController obj) {
        int id = obj.getFactionId();
        if(!homebaseOwners.contains(id)) {
            for(SystemControlInfo info : sovContainer.getAllFactionLocations(id)){
                if(info.homebaseOwners.contains(id))
                    info.homebaseOwners.remove(id);
            }
            addHomebaseOwner(id);
            homebaseUIDs.add(obj.getUniqueIdentifier());
            update();
        }
    }

    public void handleBlockEntityDestruction(SegmentController entity, int attacker, int enemy) {
        if(attacker > 0) {
            if (enemy == -1) handlePvEKill(attacker, entity);
            else if (enemy > 0 && factionIsPresent(attacker) && isWarTargetTo(attacker, enemy)) {
                handlePvPKill(attacker, enemy, entity);
            }
        } else if(enemy > 0) handlePvELoss(enemy,entity);
        //Otherwise, it's a blue-on-blue or neutral kill.
        //You're a prick! ...or you're just cleaning up floating cores idk.
        if(entity.getType() != SHIP) removeStructure(entity.getUniqueIdentifier());
    }

    public void handlePlanetDestruction(PlanetCore core){
        boolean needsUpdate = false;
        for(FactionSystemControlSheet factionCtrl : factionSheets.values()){
            if(factionCtrl.hasPlanet(core.getUniqueIdentifier())){
                for(SegmentController plate : BIUtils.getAllPlates(core)) {
                    factionCtrl.removeFactionStructure(plate.getUniqueIdentifier());
                }
                needsUpdate = true;
            }
        }
        if(needsUpdate) update();
    }
    private boolean isWarTargetTo(int self, int other) {
        FactionManager facman = GameServerState.instance.getFactionManager();
        return facman.getRelation(self,other) == FactionRelation.RType.ENEMY;
    }

    private void handlePvEKill(int attacker, SegmentController destroyed) {
        try {
            if (destroyed.getType() == SHIP) {
                neutralInfluenceFromPVE -= Math.min(NEUTRAL_PVE_BATTLE_INFLUENCE_LOSS, neutralInfluenceFromPVE); //can't go below zero
            }
            float mass = destroyed.getMassWithDocks();
            if (mass > MINIMUM_WRECK_MASS_FOR_GLORY_AWARD && getFactionInfluence(attacker, true) > MINIMUM_INFLUENCE_POINTS_FOR_GLORY_ACCESS) {
                String entityType = destroyed.getTypeString();
                giveUnclaimedGloryPvE(attacker, destroyed.getType() == SPACE_STATION ? PVE_GLORY_PER_STATION_KILL : PVE_GLORY_PER_KILL);
                BIUtils.sendAllMembersMessage("Your faction has earned Glory from the destruction of the enemy " + entityType + destroyed.getName() + "!\r\n" +
                        "This increases your Influence within the system!", attacker, MESSAGE_TYPE_INFO, destroyed.getSector(new Vector3i()));
            }
        } catch (NullPointerException ex){
            System.err.println("[BastionInitiative][ERROR] Error handling PvE kill!");
            ex.printStackTrace();
        }
    }

    private int getFactionInfluence(int attacker, boolean countAdjacency) {
        FactionSystemControlSheet sheet = getFactionSheet(attacker);
        if(sheet == null) return 0;
        else return sheet.getInfluence(countAdjacency);
    }

    @NotImplemented
    private void handlePvELoss(int npcFac, SegmentController destroyed) {
        //TODO return glory to pool? idk
    }

    private void handlePvPKill(int attacker, int enemy, SegmentController destroyed) {
        if(!destroyed.railController.isDocked() &&
                destroyed.getMassWithDocks() > MINIMUM_WRECK_MASS_FOR_GLORY_AWARD &&
                getFactionSheet(attacker).getInfluence(true) > MINIMUM_INFLUENCE_POINTS_FOR_GLORY_ACCESS)
        {
            String entityType = destroyed.getTypeString();
            giveGloryPvP(attacker, enemy, destroyed.getType() == SPACE_STATION? PVP_GLORY_PER_STATION_KILL:PVP_GLORY_PER_KILL);
            BIUtils.sendAllMembersMessage("Your faction has earned Glory from the destruction of the enemy " + entityType + destroyed.getName() + "!\r\n" +
                    "This increases your Influence within the system!" ,attacker,MESSAGE_TYPE_INFO,destroyed.getSector(new Vector3i()));
        }
    }

    public void afterDeserialize() {
        for(FactionSystemControlSheet sheet : factionSheets.values()){
            sheet.setParent(this);
            sheet.afterDeserialize();
        }
        update();
    }

    public int getInfluence(int factionID) {
        return factionSheets.get(factionID).getInfluence(true);
    }
}

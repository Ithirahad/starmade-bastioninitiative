package org.ithirahad.bastioninitiative.persistence;

import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.ithirahad.bastioninitiative.util.TemporalShortcuts;

import java.io.Serializable;
import java.util.Iterator;

import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.BastionInitiative.sovContainer;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;

public class VirtualStellarNexus extends VirtualAegisSubsystem implements Serializable {
    private float lastNetworkStructurePoints = 0;
    /**
    A map of structures with Aegis Systems currently linking, to the time when they will be fully linked.
     */
    public final Object2LongOpenHashMap<String> linking;
    /**
     A list of UIDs of structures with linked Aegis Systems.
     */
    public final ObjectOpenHashSet<String> linked;
    /**
     * Is this Stellar Nexus colliding with other nexus?
     */
    private boolean conflicting;
    private long disturbedUntil;
    private int lastEnhancement;
    private long lastDisable;
    private boolean prevPowerState;

    public VirtualStellarNexus() {
        super();
        linking = new Object2LongOpenHashMap<>();
        linked = new ObjectOpenHashSet<>();
        disturbedUntil = 0;
        conflicting = false;
        prevPowerState = false;
    }

    @Override
    public String getBlockEntryName() {
        return "Aegis Stellar Nexus";
    }

    @Override
    public void onLoadFromPersistence() {
        update();
    }

    @Override
    protected void onUpdate(boolean powered) {
        conflicting = false;
        VirtualStellarNexus other;
        int origLinked = linked.size();

        for(VirtualAegisSystem asys : bsiContainer.getAegisSystemsInSystem(getParent().getStarSystemLocation())){
            String uid = asys.uid;
            if(!this.getParent().uid.equals(uid) && asys.getFactionID() == getFactionID()) {
                other = (VirtualStellarNexus) asys.getSubsystem(getBlockEntryName());
                if (other != null && other.getParent().isActive()) {
                    conflicting = true;
                    removeAnyConnection(uid);
                } else if(!isLinkingOrLinked(asys.uid)) addLinking(asys.uid);
            }
        }

        if(noConflictingNexus()) {
            long now = System.currentTimeMillis();
            if(now > disturbedUntil)
                for (ObjectIterator<String> iterator = linking.keySet().iterator(); iterator.hasNext(); ) {
                    String key = iterator.next();
                    VirtualAegisSystem sys = bsiContainer.getAegisSystemByID(key);
                    if (sys != null && sys.getFactionID() == this.getFactionID()) {
                        if (linking.get(key) < now) {
                            addLinked(key);
                            iterator.remove();
                        }
                    } else iterator.remove();
                }
        }

        float origStructurePts = lastNetworkStructurePoints;
        lastNetworkStructurePoints = 0;
        for (ObjectIterator<String> iterator = linked.iterator(); iterator.hasNext(); ) {
            String uid = iterator.next();
            VirtualAegisSystem sys = bsiContainer.getAegisSystemByID(uid);
            if (sys != null && sys.getFactionID() == this.getFactionID()) {
                sys.setNexusChargeCostMultiplier(getChargeCostFactor());
                lastNetworkStructurePoints += sys.getLastStructurePoints();
            } else iterator.remove();
        }

        if(linked.size() != origLinked || powered != prevPowerState || lastNetworkStructurePoints != origStructurePts){
            System.err.println("[BastionInitiative][DEBUG] Stellar Nexus state change prompted system control update...");
            updateSystemControl();
        }
        prevPowerState = powered;
    }

    private SystemControlInfo getFactionInfo() {
        return sovContainer.getOrCreateInfo(getParent().getStarSystemLocation());
    }

    public float getChargeCostFactor() {
        float discountable = 1.0f - AEGIS_NEXUS_DISCOUNT_CAP;
        int enhancers = getLastEnhancement();
        float discount = discountable * ((-1/(AEGIS_NEXUS_ENHANCERS_COEFF*enhancers+1)) + 1);
        return 1.0f - discount;
    }

    private int getLastEnhancement() {
        return lastEnhancement;
    }

    private void addLinked(String uid) {
        linked.add(uid);
        VirtualAegisSystem sys = bsiContainer.getAegisSystemByID(uid);
        sys.update();
        updateSystemControl();
    }

    public void forceAllLinkingToLinked(){
        for(String uid : linking.keySet()){
            addLinked(uid);
        }
        linking.clear();
        updateSystemControl();
    }

    private boolean isLinkingOrLinked(String uid) {
        return linked.contains(uid) || linking.containsKey(uid);
    }

    private void removeLinking(String uid) {
        if(linking.containsKey(uid)) {
            linking.remove(uid);
            update();
        }
    }

    public float getNetworkTotalStructurePoints() {
        return lastNetworkStructurePoints;
    }

    public void disturb(){
        disturbedUntil = getParent().getNextCycleTime().getTimeInMillis();
        disturbedUntil += TemporalShortcuts.MS_TO_DAYS * AEGIS_NEXUS_LOCKOUT_FULL_CYCLES;
    }

    public long getDisturbedUntil(){
        return disturbedUntil;
    }

    public void addLinking(String uid){
        long until = System.currentTimeMillis();
        until += (long) (TemporalShortcuts.MS_TO_DAYS * AEGIS_NEXUS_LINKING_TIME_DAYS);
        linking.add(uid,until);

        VirtualAegisSystem sys = bsiContainer.getAegisSystemByID(uid);
        sys.setNexusLinkedTo(getParent().uid);
    }

    @Override
    public boolean isOperating(boolean respectDisruptionStatus) {
        boolean notConflicting = noConflictingNexus();
        boolean sysOperating = super.isOperating(respectDisruptionStatus);
        if(!sysOperating && !getPowered()){
            System.err.println("[BastionInitiative][DEBUG]Nexus reporting non-operational due to no power");
        }
        return sysOperating && notConflicting;
    }

    private boolean noConflictingNexus() {
        return !conflicting;
    }

    public int getNetworkSize() {
        return linked.size();
    }

    public int getAmountQueued(){
        return linking.size();
    }

    public boolean hasLinked(String uid) {
        return linked.contains(uid);
    }

    public void removeLinked(String uid) {
        if(linked.contains(uid)){
            VirtualAegisSystem v = bsiContainer.getAegisSystemByID(uid);
            v.update(); //update with discounted cost until now

            linked.remove(uid);
            update();

            updateSystemControl();
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
        lastDisable = now();
        updateSystemControl();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        long downtime = now() - lastDisable;
        for(String key : linking.keySet()){
            Long l = linking.get(key);
            if(l != null && lastDisable < l) {
                linking.put(key,l + downtime);
            }
        }
        update();
        updateSystemControl();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearConnectionsAndUpdate();
    }

    public void clearConnectionsAndUpdate(){
        Iterator<String> iterator = linked.iterator();
        while(iterator.hasNext()) {
            String uid = iterator.next();
            VirtualAegisSystem v = bsiContainer.getAegisSystemByID(uid);
            v.update(); //update with discounted cost until now

            iterator.remove();
            update();
        }

        updateSystemControl();
    }

    private void updateSystemControl() {
        getFactionInfo().update();
    }

    public void removeAnyConnection(String uid){
        removeLinking(uid);
        removeLinked(uid);
    }

    public void setEnhanced(int enhancement) {
        lastEnhancement = enhancement;
    }

    public boolean hasAnyConnection(String uid) {
        return linking.containsKey(uid) || linked.contains(uid);
    }

    public boolean hasAnyLinked() {
        return !linked.isEmpty();
    }
}

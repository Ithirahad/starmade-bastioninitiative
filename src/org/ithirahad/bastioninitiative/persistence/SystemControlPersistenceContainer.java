package org.ithirahad.bastioninitiative.persistence;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectCollections;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.server.data.GameServerState;

import java.io.Serializable;
import java.util.*;

import static java.util.Collections.EMPTY_SET;

public class SystemControlPersistenceContainer implements Serializable {
    private final HashMap<Vector3i, SystemControlInfo> factionControlByLocation;
    private transient Int2ObjectOpenHashMap<ObjectOpenHashSet<SystemControlInfo>> factionControlByFaction;

    public SystemControlPersistenceContainer(){
        factionControlByLocation = new HashMap<>();
        factionControlByFaction = new Int2ObjectOpenHashMap<>();
    }
    public void afterDeserialize(){
        factionControlByFaction = new Int2ObjectOpenHashMap<>();
        for(SystemControlInfo info : factionControlByLocation.values()){
            info.afterDeserialize();
            for(int i : info.getPresentFactions()){
                ObjectOpenHashSet<SystemControlInfo> infos = factionControlByFaction.get(i);
                if(infos == null) {
                    infos = new ObjectOpenHashSet<>();
                    factionControlByFaction.put(i, infos);
                }
                infos.add(info);
            }
        } //populate transient map

        FactionManager facman = GameServerState.instance.getFactionManager();

        for(Faction f : facman.getFactionCollection()){
            if(!f.isPlayerFaction()) continue;

            List<Vector3i> ownedSystems = GameServerState.instance.getDatabaseIndex().getTableManager().getSystemTable().getSystemsByFaction(f.getIdFaction(), (List<Vector3i>) MiscUtils.readPrivateField("lastSystems",f), f.lastSystemSectors, (List<String>) MiscUtils.readPrivateField("lastSystemUIDs",f));
            for(Vector3i v : ownedSystems){
                boolean remove = false;
                if(!factionControlByFaction.containsKey(f.getIdFaction())){
                    System.err.println("[BastionInitiative][WARNING] Removing " + f.getName() + " ownership of system " + v + ": No record of faction earning territory!");
                    remove = true;
                }
                else{
                    SystemControlInfo info = factionControlByLocation.get(v);
                    if(info == null){
                       System.err.println("[BastionInitiative][WARNING] Removing " + f.getName() + " ownership of system " + v + ": No record of faction activity here!");
                       remove = true;
                    }
                    else if(info.getControllingFactionID() != f.getIdFaction()){
                        info.update();
                        System.err.println("[BastionInitiative][WARNING] Forced ownership change of system " + v + ": " + f.getName() + " has no right to this system!");
                        try {
                            BIUtils.setFactionOwnershipWithoutClaimStation(info.getControllingFactionID(),v,"SYSTEM FAILSAFE",false);
                        } catch (Exception ex){
                            System.err.println("[BastionInitiative][ERROR] Could not remove invalid faction ownership of system " + v + ":");
                            ex.printStackTrace();
                        }
                    }
               }

               if(remove) try {
                   BIUtils.setFactionOwnershipWithoutClaimStation(0,v,"SYSTEM FAILSAFE",false);
               } catch (Exception ex){
                   System.err.println("[BastionInitiative][ERROR] Could not remove invalid faction ownership of system " + v + ":");
                   ex.printStackTrace();
               }
           }
        }
    }
    public FactionSystemControlSheet addFactionToSystem(Vector3i system, int factionID) {
        if (GameServerState.instance.getFactionManager().existsFaction(factionID)) {
            SystemControlInfo systemInfo;
            if (factionControlByLocation.containsKey(system)) {
                systemInfo = factionControlByLocation.get(system);
            } else {
                systemInfo = new SystemControlInfo(system);
                factionControlByLocation.put(new Vector3i(system),systemInfo);
            }

            systemInfo.putFaction(factionID);
            if(!factionControlByFaction.containsKey(factionID)) factionControlByFaction.put(factionID,new ObjectOpenHashSet<SystemControlInfo>());
            factionControlByFaction.get(factionID).add(systemInfo);

            return systemInfo.getFactionSheet(factionID);
        }
        else {
            System.err.println("[MOD][BastionInitiative][ERROR] Attempted to add nonexistent faction to system!");
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            for (int i = 0, stackTraceLength = stackTrace.length; i < stackTraceLength; i++) {
                StackTraceElement s = stackTrace[i];
                System.err.print("  " + s.toString());
            }
            return null; //TODO: bad error handling, I know.
        }
    }

    public void removeFactionFromSystem(Vector3i system, int factionID){
        if(hasFactionAt(system,factionID)){
            SystemControlInfo systemInfo = factionControlByLocation.get(system);
            systemInfo.getFactionSheet(factionID).reset();
            systemInfo.removeFaction(factionID);
            Set factionHoldings = factionControlByFaction.get(factionID);
            factionHoldings.remove(system);
            if(factionHoldings.isEmpty()) factionControlByFaction.remove(factionID);
            if(systemInfo.isEmpty()) resetSystem(system);
        }
    }

    public void resetSystem(Vector3i system){
        SystemControlInfo info = factionControlByLocation.get(system);
        info.clear();
        factionControlByLocation.remove(system);
    }

    public FactionSystemControlSheet getFactionSheet(int factionID, Vector3i v) {
        if(factionControlByLocation.containsKey(v)){
            return factionControlByLocation.get(v).getFactionSheet(factionID);
        } else return null;
    }

    public SystemControlInfo getOrCreateInfo(Vector3i starSystem) {
        if(factionControlByLocation.containsKey(starSystem)) return factionControlByLocation.get(starSystem);
        else{ //have to make a new one
            SystemControlInfo result = new SystemControlInfo(starSystem);
            factionControlByLocation.put(new Vector3i(starSystem),result);
            return result;
        }
    }

    public SystemControlInfo getInfo(Vector3i starSystem) {
        return factionControlByLocation.get(starSystem);
    }

    public boolean hasFactionAt(Vector3i starSystem, int faction) {
        return faction != 0 && factionControlByFaction.containsKey(faction) && factionControlByFaction.get(faction).contains(factionControlByLocation.get(starSystem));
    }

    public int getSystemOwner(Vector3i pos) {
        if(!factionControlByLocation.containsKey(pos)) return BIUtils.isStar(pos)? -1 : 0;
        else{
            SystemControlInfo info = factionControlByLocation.get(pos);
            return info.getControllingFactionID();
        }
    }

    public void beforeSerialize() {
        //TODO clean up any nonexistent UIDs from structure tracking and update all, then clean up unmodified systems
    }


    private static final Collection<SystemControlInfo> EMPTY = new ArrayList<>();
    /**
     * @param id The faction's numeric ID.
     * @return The system control info for all systems where a faction has any influence.
     */
    public Collection<SystemControlInfo> getAllFactionLocations(int id) {
        if(!factionControlByFaction.containsKey(id)){
            synchronized(EMPTY) {
                EMPTY.clear();
                return EMPTY; //produces more succinct code with no silly null checks
            }
        }
        return factionControlByFaction.get(id);
    }
}

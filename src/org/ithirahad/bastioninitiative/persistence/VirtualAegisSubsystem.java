package org.ithirahad.bastioninitiative.persistence;

import java.io.Serializable;

import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.VULNERABLE;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;

public abstract class VirtualAegisSubsystem implements Serializable {
    /**
     * The parent PersistentAegisSystem.
     */
    private transient VirtualAegisSystem parent;
    private boolean powered;

    public VirtualAegisSubsystem(){}

    public int getFactionID() {
        return parent.getFactionID();
    }

    public abstract String getBlockEntryName();

    public abstract void onLoadFromPersistence();

    protected abstract void onUpdate(boolean powered);

    public final VirtualAegisSystem getParent(){
        return parent;
    }

    public final void update(){
        onUpdate(powered);
    }

    public final void setPowered(boolean powered){
        if(powered != this.powered) {
            this.powered = powered;
            System.err.println("[BastionInitiative][DEBUG] Virtual " + getBlockEntryName() + " set powered to " + powered + "; updating...");
            update(); //this is likely to change important things.
        }
    };

    public boolean getPowered(){
        return powered;
    };

    public boolean isOperating(boolean respectDisruptionStatus){
        if(parent == null) return false;

        boolean active;
        if(respectDisruptionStatus) active = parent.isActive();
        else active = parent.isActive() || parent.getStatus() == VULNERABLE;
        boolean hasFuel = now() < parent.getFuelledUntil();
        return powered && active && hasFuel;
    }

    public final void setParent(VirtualAegisSystem parent){
        this.parent = parent;
    };

    public void onDestroy(){
        parent = null;
    }

    public void onDisable() {
    }

    public void onEnable() {
    }
}

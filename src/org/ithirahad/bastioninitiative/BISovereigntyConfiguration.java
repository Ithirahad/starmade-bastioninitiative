package org.ithirahad.bastioninitiative;

public class BISovereigntyConfiguration {
    /**
     * Minimum influence points for a system to share a fraction its influence to nearby systems.
     */
    public static int MINIMUM_LOCAL_INFLUENCE_POINTS_FOR_ADJACENCY_BONUS = 50;

    /**
     * How much of a system's influence points are added to neighbouring systems by default
     */
    public static float ADJACENCY_BONUS_BASE_MULTIPLIER = 0.2f;

    /**
     * Influence points gained from being the first faction currently owning a homebase in the system
     */
    public static int INFLUENCE_FROM_FIRST_HOMEBASE = 60;

    /**
     * Flat influence points gained for owning an active stellar nexus
     */
    public static int INFLUENCE_BASE_FROM_STELLAR_NEXUS = 10;

    /**
     * Influence points gained for each station/planet linked to the Stellar Nexus via an aegis core, based on the amount of Structure Points on the linked structures
     */
    public static float INFLUENCE_FROM_STELLAR_NEXUS_PER_LINKED_STRUCT_POINT = 0.25f;

    /**
     * Influence points for having any static asset claimed in the star system.
     */
    public static int INFLUENCE_FROM_ANY_CLAIMED_STRUCTURE_STARSYSTEM = 30;

    /**
     * Influence points for having any static asset claimed in the void system block.
     */
    public static int INFLUENCE_POINTS_FROM_ANY_CLAIMED_STRUCTURE_VOID = 10;

    /**
     * Influence points per claimed planet in a system.
     */
    public static int INFLUENCE_PER_OWN_PLANET_BASE = 10;

    /**
     * The default amount of neutral influence that's always present in a <b>star system</b>.<br/>
     * This value is effectively the amount of influence points any player faction must exceed in order to claim the system.
     */
    public static int NEUTRAL_INFLUENCE_BASE_STARSYSTEM = 50;

    /**
     * The default amount of neutral influence that's always present in a <b>void system</b>.<br/>
     * This value is effectively the amount of influence points any player faction must exceed in order to claim the block of void space.
     */
    public static int NEUTRAL_INFLUENCE_BASE_VOID = 10;

    /**
     * The amount of neutral influence that can be reduced by destroying/unclaiming pirate structures.
     * This mechanic is ignored in the void.
     */
    public static int NEUTRAL_PVE_STRUCTURE_INFLUENCE_POOL_STARSYSTEM = 30;

    /**
     * Amount of neutral influence lost from the relevant pool, per unclaimed or destroyed pirate structure.<br/>
     * This does not continue indefinitely; the maximum amount of neutral influence that can be lost is equal to NEUTRAL_PVE_STRUCTURE_INFLUENCE_POOL_STARSYSTEM.<br/>
     * This mechanic is ignored in the void.
     */
    public static int NEUTRAL_PVE_STRUCTURE_UNCLAIM_LOSS = 5;

    /**
     * Amount of neutral influence potentially lost in ship combat.
     */
    public static int NEUTRAL_PVE_BATTLE_INFLUENCE_POOL_STARSYSTEM = 20;


    /**
     * Amount of neutral influence lost for a destroyed pirate ship.
     */
    public static int NEUTRAL_PVE_BATTLE_INFLUENCE_LOSS = 2;

    /**
     *
     */
    public static int NEUTRAL_PVE_BATTLE_INFLUENCE_REGEN_PER_DAY = 2;

    /**
     * Total fleet mass required to count for fleet influence bonus
     */
    public static int MASS_THRESHOLD_FOR_FLEET_INFLUENCE = 400;

    /**
     * Total fleet member count required to count for fleet influence bonus
     */
    public static int MINIMUM_MEMBERS_FOR_FLEET_INFLUENCE = 2;

    /**
     * Bonus influence for having at least one fleet present and active (defending/attacking/patrolling/etc.) within a <b>star system</b>.<br/>
     * This mechanic is ignored in the void.
     */
    public static int INFLUENCE_FROM_ANY_ACTIVE_FLEET_STARSYSTEM = 20;


    /**
     * Minimum influence in order to be able to earn influence from a system's available pool of Glory points
     */
    public static int MINIMUM_INFLUENCE_POINTS_FOR_GLORY_ACCESS = 30;

    public static float MINIMUM_WRECK_MASS_FOR_GLORY_AWARD = 50f;

    /**
     * Total amount of Glory up for grabs in a star/void system.
     */
    public static int SYSTEM_GLORY_POOL = 100;

    public static int PVE_GLORY_PER_KILL = 2;

    public static int PVE_GLORY_PER_STATION_KILL = 10;

    public static int PVP_GLORY_PER_KILL = 5;

    public static int PVP_GLORY_PER_STATION_KILL = 10;

    public static float INFLUENCE_PER_CLAIMED_GLORY = 10.0f;

    /**
     * Threshold below which a faction is considered to have low faction points
     */
    public static int LOW_FACTIONPOINTS_THRESHOLD = 150;

    /**
     * Global influence multiplier for a faction having low faction points
     */
    public static float LOW_FACTIONPOINTS_INFLUENCE_MODIFIER = 0.6f;

    /**
     * Global influence multiplier for a faction having negative faction points
     */
    public static float NEGATIVE_FACTIONPOINTS_INFLUENCE_MODIFIER = 0.25f;
}
